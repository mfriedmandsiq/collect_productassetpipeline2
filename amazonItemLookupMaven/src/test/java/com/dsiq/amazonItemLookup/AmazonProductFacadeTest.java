package com.dsiq.amazonItemLookup;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.easymock.EasyMock;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.easymock.PowerMock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

//http://code.google.com/p/powermock/wiki/MockitoUsage13
@RunWith(PowerMockRunner.class)
@PrepareForTest({ DriverManager.class, AmazonProductFacade.class })
public class AmazonProductFacadeTest {

	@Test
	public void testProductFacadeReserveSuccess() throws SQLException {

		// create connections for reserve(),process(), complete()
		Connection mockConn = mock(Connection.class);
		Connection mockConnProcess = mock(Connection.class);
		Connection mockConnComplete = mock(Connection.class);

		// http://stackoverflow.com/questions/19464975/how-to-mock-drivermanager-getconnection
		// Prepare connections, one for each call to reserve(),process(),or
		// complete()

		PowerMock.mockStatic(Class.class);
		try {
			EasyMock.expect(Class.forName(EasyMock.<String> anyObject()))
					.andReturn(null).andReturn(null).andReturn(null);

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		PowerMock.replay(Class.class);

		PowerMock.mockStatic(DriverManager.class);
		// connections
		EasyMock.expect(DriverManager.getConnection("test"))
				.andReturn(mockConn); // for first reserve()
		EasyMock.expect(DriverManager.getConnection("test")).andReturn(
				mockConnProcess); // for first process()
		EasyMock.expect(DriverManager.getConnection("test")).andReturn(
				mockConnComplete); // for first complete()
		// wrap up connection mocking
		EasyMock.expect(DriverManager.getConnection(null)).andReturn(null);
		PowerMock.replay(DriverManager.class);

		// for first reserve() call
		final Statement mockStatement = mock(Statement.class);

		// for updateStatus call in first reserve() call
		final PreparedStatement mockPreparedStatement = mock(PreparedStatement.class);
		when(mockConn.createStatement()).thenReturn(mockStatement);
		when(mockConn.prepareStatement(anyString())).thenReturn(
				mockPreparedStatement);
		when(mockPreparedStatement.execute()).thenReturn(true);
		when(mockPreparedStatement.getUpdateCount()).thenReturn(1);

		// for updateStatus in first process() call
		final PreparedStatement mockPreparedStatementProcessing = mock(PreparedStatement.class);
		when(mockConnProcess.prepareStatement(anyString())).thenReturn(
				mockPreparedStatementProcessing);
		when(mockConnProcess.prepareStatement(anyString())).thenReturn(
				mockPreparedStatementProcessing);
		when(mockPreparedStatementProcessing.execute()).thenReturn(true);
		when(mockPreparedStatementProcessing.getUpdateCount()).thenReturn(1);

		// for updateStatus in first complete() call
		final PreparedStatement mockPreparedStatementComplete = mock(PreparedStatement.class);
		when(mockConnComplete.prepareStatement(anyString())).thenReturn(
				mockPreparedStatementComplete);
		when(mockConnComplete.prepareStatement(anyString())).thenReturn(
				mockPreparedStatementComplete);
		when(mockPreparedStatementComplete.execute()).thenReturn(false);
		when(mockPreparedStatementComplete.getUpdateCount()).thenReturn(1);

		final AmazonProductFacade productFacade = new AmazonProductFacade("test");

		// prepping mockRSet for first reserve() call
		ResultSet mockRSet = mock(ResultSet.class);
		when(mockStatement.executeQuery(anyString())).thenReturn(mockRSet);
		when(mockRSet.getString("LAT")).thenReturn("40.92233", "18.16527");
		when(mockRSet.getString("LON")).thenReturn("-72.6371", "-66.7226");
		when(mockRSet.next()).thenReturn(true);

		// test reserve
		AmazonProduct product = productFacade.reserve();
		assertNull(product);
	}

	@Test
	public void testProductFacadeCompleteReserved() throws SQLException {

		// create connections for reserve(),process(), complete()
		Connection mockConn = mock(Connection.class);
		Connection mockConnProcess = mock(Connection.class);
		Connection mockConnComplete = mock(Connection.class);
		Connection mockConnComplete2 = mock(Connection.class);

		// http://stackoverflow.com/questions/19464975/how-to-mock-drivermanager-getconnection
		// Prepare connections, one for each call to reserve(),process(),or
		// complete()
		PowerMock.mockStatic(Class.class);
		try {
			EasyMock.expect(Class.forName(EasyMock.<String> anyObject()))
					.andReturn(null).andReturn(null).andReturn(null);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		PowerMock.replay(Class.class);

		PowerMock.mockStatic(DriverManager.class);
		// connections
		EasyMock.expect(DriverManager.getConnection("test"))
				.andReturn(mockConn); // for first reserve()
		EasyMock.expect(DriverManager.getConnection("test")).andReturn(
				mockConnProcess); // for first process()
		EasyMock.expect(DriverManager.getConnection("test")).andReturn(
				mockConnComplete); // for first complete()
		EasyMock.expect(DriverManager.getConnection("test")).andReturn(
				mockConnComplete2); // for second complete()
		// wrap up connection mocking
		EasyMock.expect(DriverManager.getConnection(null)).andReturn(null);
		PowerMock.replay(DriverManager.class);

		// for first reserve() call
		final Statement mockStatement = mock(Statement.class);

		// for updateStatus call in first reserve() call
		final PreparedStatement mockPreparedStatement = mock(PreparedStatement.class);
		when(mockConn.createStatement()).thenReturn(mockStatement);
		when(mockConn.prepareStatement(anyString())).thenReturn(
				mockPreparedStatement);
		when(mockPreparedStatement.execute()).thenReturn(true);
		when(mockPreparedStatement.getUpdateCount()).thenReturn(1);

		// for updateStatus in first process() call
		final PreparedStatement mockPreparedStatementProcessing = mock(PreparedStatement.class);
		when(mockConnProcess.prepareStatement(anyString())).thenReturn(
				mockPreparedStatementProcessing);
		when(mockConnProcess.prepareStatement(anyString())).thenReturn(
				mockPreparedStatementProcessing);
		when(mockPreparedStatementProcessing.execute()).thenReturn(true);
		when(mockPreparedStatementProcessing.getUpdateCount()).thenReturn(1);

		// for updateStatus in first complete() call
		final PreparedStatement mockPreparedStatementComplete = mock(PreparedStatement.class);
		when(mockConnComplete.prepareStatement(anyString())).thenReturn(
				mockPreparedStatementComplete);
		when(mockConnComplete.prepareStatement(anyString())).thenReturn(
				mockPreparedStatementComplete);
		when(mockPreparedStatementComplete.execute()).thenReturn(true);
		when(mockPreparedStatementComplete.getUpdateCount()).thenReturn(1);

		// for updateStatus in second complete() call
		final PreparedStatement mockPreparedStatementComplete2 = mock(PreparedStatement.class);
		when(mockConnComplete2.prepareStatement(anyString())).thenReturn(
				mockPreparedStatementComplete2);
		when(mockConnComplete2.prepareStatement(anyString())).thenReturn(
				mockPreparedStatementComplete2);
		when(mockPreparedStatementComplete2.execute()).thenReturn(true);
		when(mockPreparedStatementComplete2.getUpdateCount()).thenReturn(1);

		@SuppressWarnings("unused")
		final AmazonProductFacade productFacade = new AmazonProductFacade("test");

		// prepping mockRSet for first reserve() call
		ResultSet mockRSet = mock(ResultSet.class);
		when(mockStatement.executeQuery(anyString())).thenReturn(mockRSet);
		when(mockRSet.getString("LAT")).thenReturn("40.92233", "18.16527");
		when(mockRSet.getString("LON")).thenReturn("-72.6371", "-66.7226");
		when(mockRSet.next()).thenReturn(true);

		/*
		 * // make sure that trying to complete a product that is not processing
		 * 
		 * try { productFacade.complete(product); fail("Invalid state"); }
		 * catch (IllegalStateException e) {
		 * 
		 * }
		 */
	}

	@Test
	public void testProductFacadeCompleteStaleProcessing() throws SQLException {

		// create connections for reserve(),process(), complete()
		Connection mockConn = mock(Connection.class);
		Connection mockConnProcess = mock(Connection.class);
		Connection mockConnComplete = mock(Connection.class);
		Connection mockConnComplete2 = mock(Connection.class);

		// http://stackoverflow.com/questions/19464975/how-to-mock-drivermanager-getconnection
		// Prepare connections, one for each call to reserve(),process(),or
		// complete()
		PowerMock.mockStatic(Class.class);
		try {
			EasyMock.expect(Class.forName(EasyMock.<String> anyObject()))
					.andReturn(null).andReturn(null).andReturn(null);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		PowerMock.replay(Class.class);

		PowerMock.mockStatic(DriverManager.class);
		// connections
		EasyMock.expect(DriverManager.getConnection("test"))
				.andReturn(mockConn); // for first reserve()
		EasyMock.expect(DriverManager.getConnection("test")).andReturn(
				mockConnProcess); // for first process()
		EasyMock.expect(DriverManager.getConnection("test")).andReturn(
				mockConnComplete); // for first complete()
		EasyMock.expect(DriverManager.getConnection("test")).andReturn(
				mockConnComplete2); // for second complete()
		// wrap up connection mocking
		EasyMock.expect(DriverManager.getConnection(null)).andReturn(null);
		PowerMock.replay(DriverManager.class);

		// for first reserve() call
		final Statement mockStatement = mock(Statement.class);

		// for updateStatus call in first reserve() call
		final PreparedStatement mockPreparedStatement = mock(PreparedStatement.class);
		when(mockConn.createStatement()).thenReturn(mockStatement);
		when(mockConn.prepareStatement(anyString())).thenReturn(
				mockPreparedStatement);
		when(mockPreparedStatement.execute()).thenReturn(true);
		when(mockPreparedStatement.getUpdateCount()).thenReturn(1);

		// for updateStatus in first process() call
		final PreparedStatement mockPreparedStatementProcessing = mock(PreparedStatement.class);
		when(mockConnProcess.prepareStatement(anyString())).thenReturn(
				mockPreparedStatementProcessing);
		when(mockConnProcess.prepareStatement(anyString())).thenReturn(
				mockPreparedStatementProcessing);
		when(mockPreparedStatementProcessing.execute()).thenReturn(true);
		when(mockPreparedStatementProcessing.getUpdateCount()).thenReturn(1);

		// for updateStatus in first complete() call
		final PreparedStatement mockPreparedStatementComplete = mock(PreparedStatement.class);
		when(mockConnComplete.prepareStatement(anyString())).thenReturn(
				mockPreparedStatementComplete);
		when(mockConnComplete.prepareStatement(anyString())).thenReturn(
				mockPreparedStatementComplete);
		when(mockPreparedStatementComplete.execute()).thenReturn(true);
		when(mockPreparedStatementComplete.getUpdateCount()).thenReturn(1);

		// for updateStatus in second complete() call
		final PreparedStatement mockPreparedStatementComplete2 = mock(PreparedStatement.class);
		when(mockConnComplete2.prepareStatement(anyString())).thenReturn(
				mockPreparedStatementComplete2);
		when(mockConnComplete2.prepareStatement(anyString())).thenReturn(
				mockPreparedStatementComplete2);
		when(mockPreparedStatementComplete2.execute()).thenReturn(true);
		when(mockPreparedStatementComplete2.getUpdateCount()).thenReturn(1);

		final AmazonProductFacade productFacade = new AmazonProductFacade("test");

		// prepping mockRSet for first reserve() call
		ResultSet mockRSet = mock(ResultSet.class);
		when(mockStatement.executeQuery(anyString())).thenReturn(mockRSet);
		when(mockRSet.getString("LAT")).thenReturn("40.92233", "18.16527");
		when(mockRSet.getString("LON")).thenReturn("-72.6371", "-66.7226");
		when(mockRSet.next()).thenReturn(true);

		// test that trying to complete a stale processing product fails
		AmazonProduct product = productFacade.reserve();
		assertNull(product);

		/*
		 * productFacade.process(product); Long originalExpirationDateTime =
		 * product.getExpirationDate().getTime(); Date testExpirationTime = new
		 * Date(originalExpirationDateTime - (new Long(100000000)));
		 * product.setExpirationDate(testExpirationTime); try {
		 * productFacade.complete(product); fail(); } catch
		 * (IllegalStateException e) {
		 * 
		 * }
		 */
	}

	@Test
	public void testProductFacadeProcessStaleReserved() throws SQLException {

		// create connections for reserve(),process(), complete()
		Connection mockConn = mock(Connection.class);
		Connection mockConnProcess = mock(Connection.class);
		Connection mockConnComplete = mock(Connection.class);
		Connection mockConnComplete2 = mock(Connection.class);

		// http://stackoverflow.com/questions/19464975/how-to-mock-drivermanager-getconnection
		// Prepare connections, one for each call to reserve(),process(),or
		// complete()
		PowerMock.mockStatic(Class.class);
		try {
			EasyMock.expect(Class.forName(EasyMock.<String> anyObject()))
					.andReturn(null).andReturn(null).andReturn(null);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		PowerMock.replay(Class.class);

		PowerMock.mockStatic(DriverManager.class);
		// connections
		EasyMock.expect(DriverManager.getConnection("test"))
				.andReturn(mockConn); // for first reserve()
		EasyMock.expect(DriverManager.getConnection("test")).andReturn(
				mockConnProcess); // for first process()
		EasyMock.expect(DriverManager.getConnection("test")).andReturn(
				mockConnComplete); // for first complete()
		EasyMock.expect(DriverManager.getConnection("test")).andReturn(
				mockConnComplete2); // for second complete()
		// wrap up connection mocking
		EasyMock.expect(DriverManager.getConnection(null)).andReturn(null);
		PowerMock.replay(DriverManager.class);

		// for first reserve() call
		final Statement mockStatement = mock(Statement.class);

		// for updateStatus call in first reserve() call
		final PreparedStatement mockPreparedStatement = mock(PreparedStatement.class);
		when(mockConn.createStatement()).thenReturn(mockStatement);
		when(mockConn.prepareStatement(anyString())).thenReturn(
				mockPreparedStatement);
		when(mockPreparedStatement.execute()).thenReturn(true);
		when(mockPreparedStatement.getUpdateCount()).thenReturn(1);

		// for updateStatus in first process() call
		final PreparedStatement mockPreparedStatementProcessing = mock(PreparedStatement.class);
		when(mockConnProcess.prepareStatement(anyString())).thenReturn(
				mockPreparedStatementProcessing);
		when(mockConnProcess.prepareStatement(anyString())).thenReturn(
				mockPreparedStatementProcessing);
		when(mockPreparedStatementProcessing.execute()).thenReturn(true);
		when(mockPreparedStatementProcessing.getUpdateCount()).thenReturn(1);

		// for updateStatus in first complete() call
		final PreparedStatement mockPreparedStatementComplete = mock(PreparedStatement.class);
		when(mockConnComplete.prepareStatement(anyString())).thenReturn(
				mockPreparedStatementComplete);
		when(mockConnComplete.prepareStatement(anyString())).thenReturn(
				mockPreparedStatementComplete);
		when(mockPreparedStatementComplete.execute()).thenReturn(true);
		when(mockPreparedStatementComplete.getUpdateCount()).thenReturn(1);

		// for updateStatus in second complete() call
		final PreparedStatement mockPreparedStatementComplete2 = mock(PreparedStatement.class);
		when(mockConnComplete2.prepareStatement(anyString())).thenReturn(
				mockPreparedStatementComplete2);
		when(mockConnComplete2.prepareStatement(anyString())).thenReturn(
				mockPreparedStatementComplete2);
		when(mockPreparedStatementComplete2.execute()).thenReturn(true);
		when(mockPreparedStatementComplete2.getUpdateCount()).thenReturn(1);

		final AmazonProductFacade productFacade = new AmazonProductFacade("test");

		// prepping mockRSet for first reserve() call
		ResultSet mockRSet = mock(ResultSet.class);
		when(mockStatement.executeQuery(anyString())).thenReturn(mockRSet);
		when(mockRSet.getString("LAT")).thenReturn("40.92233", "18.16527");
		when(mockRSet.getString("LON")).thenReturn("-72.6371", "-66.7226");
		when(mockRSet.next()).thenReturn(true);

		// test that trying to process a stale reserved product fails
		AmazonProduct product = productFacade.reserve();
		assertNull(product);
		/*
		 * Long originalExpirationDateTime =
		 * product.getExpirationDate().getTime(); Date testExpirationTime = new
		 * Date(originalExpirationDateTime - (new Long(100000000)));
		 * product.setExpirationDate(testExpirationTime);
		 * 
		 * try { productFacade.process(product); fail(); } catch
		 * (IllegalStateException e) {
		 * 
		 * }
		 */
	}

	@Test
	public void testProductFacadeMockConnection() throws SQLException {
		Connection mockConn = mock(Connection.class);
		AmazonProductFacade mockProductFacade = mock(AmazonProductFacade.class);
		when(mockProductFacade.getConnection()).thenReturn(mockConn);
		Connection productMockConn = mockProductFacade.getConnection();
		assertEquals(mockConn, productMockConn);
	}

	@Test
	public void testProductFacadeMockitoSpyConnection() throws SQLException {
		try {
			Connection mockConn = mock(Connection.class);

			// fail to mock static method
			// http://stackoverflow.com/questions/21128690/mocking-static-methods-with-powermock-and-mockito
			PowerMock.mockStatic(Class.class);
			try {
				EasyMock.expect(Class.forName(EasyMock.<String> anyObject()))
						.andReturn(null).andReturn(null).andReturn(null);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
			PowerMock.replay(Class.class);

			PowerMockito.mockStatic(DriverManager.class);
			Mockito.when(DriverManager.getConnection(anyString())).thenReturn(
					mockConn);
			AmazonProductFacade productFacade = new AmazonProductFacade("test");
			AmazonProductFacade spyProductFacade = spy(productFacade);
			Connection conn = spyProductFacade.getConnection();
			assertEquals(mockConn, conn);
		} catch (Exception ex) {
			// should fail
		}

	}

	@Test
	public void testProductFacadeEasyMockConnection() throws SQLException {
		Connection mockConn = mock(Connection.class);

		// http://stackoverflow.com/questions/19464975/how-to-mock-drivermanager-getconnection
		PowerMock.mockStatic(Class.class);
		try {
			EasyMock.expect(Class.forName(EasyMock.<String> anyObject()))
					.andReturn(null).andReturn(null).andReturn(null);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		PowerMock.replay(Class.class);

		PowerMock.mockStatic(DriverManager.class);
		EasyMock.expect(DriverManager.getConnection("test"))
				.andReturn(mockConn);
		EasyMock.expect(DriverManager.getConnection(null)).andReturn(null);
		PowerMock.replay(DriverManager.class);

		final AmazonProductFacade productFacade = new AmazonProductFacade("test");
		Connection conn = productFacade.getConnection();
		assertEquals(mockConn, conn);

	}

	@Test
	public void testProductFacadeEasyMockConnectionStatement()
			throws SQLException {
		Connection mockConn = mock(Connection.class);
		final Statement mockStatement = mock(Statement.class);

		when(mockConn.createStatement()).thenReturn(mockStatement);

		// http://stackoverflow.com/questions/19464975/how-to-mock-drivermanager-getconnection
		PowerMock.mockStatic(Class.class);
		try {
			EasyMock.expect(Class.forName(EasyMock.<String> anyObject()))
					.andReturn(null).andReturn(null).andReturn(null);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		PowerMock.replay(Class.class);

		PowerMock.mockStatic(DriverManager.class);
		EasyMock.expect(DriverManager.getConnection("test"))
				.andReturn(mockConn);
		EasyMock.expect(DriverManager.getConnection(null)).andReturn(null);
		PowerMock.replay(DriverManager.class);

		final AmazonProductFacade productFacade = new AmazonProductFacade("test");
		Connection conn = productFacade.getConnection();
		assertEquals(mockConn, conn);
		assertEquals(mockStatement, conn.createStatement());
	}

}
