package com.dsiq.amazonItemLookup.Amazon;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class AmazonItemLookupTest {
	@Test
	public void testCreateURL() {
		String asin = "0000031860";
		
		String sub1 = String.format("AWSAccessKeyId=%s", AmazonItemLookup.AWS_ACCESS_KEY_ID);
		String sub2 = String.format("Service=%s", AmazonItemLookup.SERVICE);
		String sub3 = String.format("Version=%s", AmazonItemLookup.AMAZON_VERSION);
		String sub4 = String.format("Operation=%s", AmazonItemLookup.OPERATION);
		String sub5 = String.format("ItemId=%s", asin);
		String sub6 = String.format("ResponseGroup=%s", AmazonItemLookup.RESPONSE_GROUP);
		String sub7 = String.format("AssociateTag=%s", AmazonItemLookup.TAG);
		
		String sub8 = "&Signature=";
		String sub9 = "&Timestamp=";
		
		String actual = AmazonItemLookup.ItemLookupURL(asin);
		assertTrue(actual.contains(AmazonItemLookup.ENDPOINT));
		assertTrue(actual.contains(sub1));
		assertTrue(actual.contains(sub2));
		assertTrue(actual.contains(sub3));
		assertTrue(actual.contains(sub4));
		assertTrue(actual.contains(sub5));
		assertTrue(actual.contains(sub6));
		assertTrue(actual.contains(sub7));
		assertTrue(actual.contains(sub8));
		assertTrue(actual.contains(sub9));
	}
}
