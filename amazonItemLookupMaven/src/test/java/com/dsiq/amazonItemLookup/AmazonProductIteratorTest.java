package com.dsiq.amazonItemLookup;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;

import org.junit.Test;

//@RunWith(PowerMockRunner.class)
//@PrepareForTest({ChainNameFacade.class})

public class AmazonProductIteratorTest {
	private String asin1 = "0000031860";
	private String asin2 = "0000031879";

	@Test
	public void testIteratorStaleReserve() throws SQLException {

		AmazonProductFacade mockProductFacade = mock(AmazonProductFacade.class);
		AmazonProductIterator spyProductIterator = spy(new AmazonProductIterator("test"));
		when(spyProductIterator.getAmazonProductFacade()).thenReturn(
				mockProductFacade);

		AmazonProduct prod1 = new AmazonProduct();
		prod1.setAsin(asin1);
		GregorianCalendar prod1GregC = new GregorianCalendar();
		prod1.setLoadDate(prod1GregC.getTime());
		prod1GregC.add(Calendar.MINUTE, 10);
		prod1.setExpirationDate(prod1GregC.getTime());

		when(mockProductFacade.reserve()).thenReturn(prod1).thenReturn(null);

		Iterator<AmazonProduct> actualIterator = spyProductIterator;
		assertTrue(actualIterator.hasNext());

		prod1GregC.add(Calendar.MINUTE, -5);
		prod1.setExpirationDate(prod1GregC.getTime());
		assertFalse(actualIterator.hasNext());

		// expire prod1, then there are no remaining chains
		prod1GregC.add(Calendar.MINUTE, -11);
		prod1.setExpirationDate(prod1GregC.getTime());
		assertFalse(actualIterator.hasNext());

	}

	@Test
	public void testIteratorTwoStaleReserve() throws SQLException {

		AmazonProductFacade mockProductFacade = mock(AmazonProductFacade.class);
		AmazonProductIterator spyProductIterator = spy(new AmazonProductIterator("test"));
		when(spyProductIterator.getAmazonProductFacade()).thenReturn(
				mockProductFacade);

		AmazonProduct prod1 = new AmazonProduct();
		prod1.setAsin(asin1);
		GregorianCalendar prod1GregC = new GregorianCalendar();
		prod1.setLoadDate(prod1GregC.getTime());
		prod1GregC.add(Calendar.MINUTE, 10);
		prod1.setExpirationDate(prod1GregC.getTime());

		AmazonProduct prod2 = new AmazonProduct();
		prod2.setAsin(asin2);
		GregorianCalendar prod2GregC = new GregorianCalendar();
		prod2.setLoadDate(prod2GregC.getTime());
		prod2GregC.add(Calendar.MINUTE, 10);
		prod2.setExpirationDate(prod2GregC.getTime());

		when(mockProductFacade.reserve()).thenReturn(prod1).thenReturn(prod2)
				.thenReturn(null);

		Iterator<AmazonProduct> actualIterator = spyProductIterator;
		assertTrue(actualIterator.hasNext());

		prod1GregC.add(Calendar.MINUTE, -5);
		prod1.setExpirationDate(prod1GregC.getTime());
		assertTrue(actualIterator.hasNext());

		prod1GregC.add(Calendar.MINUTE, -11);
		prod1.setExpirationDate(prod1GregC.getTime());

		// skip prod1, move to prod2.
		assertFalse(actualIterator.hasNext());
	}

	@Test
	public void testIteratorProcess() throws SQLException {

		AmazonProductFacade mockProductFacade = mock(AmazonProductFacade.class);
		AmazonProductIterator spyProductIterator = spy(new AmazonProductIterator("test"));
		when(spyProductIterator.getAmazonProductFacade()).thenReturn(
				mockProductFacade);

		AmazonProduct prod1 = new AmazonProduct();
		prod1.setAsin(asin1);
		GregorianCalendar prod1GregC = new GregorianCalendar();
		prod1.setLoadDate(prod1GregC.getTime());
		prod1GregC.add(Calendar.MINUTE, 10);
		prod1.setExpirationDate(prod1GregC.getTime());

		AmazonProduct prod2 = new AmazonProduct();
		prod2.setAsin(asin2);
		GregorianCalendar prod2GregC = new GregorianCalendar();
		prod2.setLoadDate(prod2GregC.getTime());
		prod2GregC.add(Calendar.MINUTE, 10);
		prod2.setExpirationDate(prod2GregC.getTime());

		when(mockProductFacade.reserve()).thenReturn(prod1).thenReturn(prod2)
				.thenReturn(null).thenReturn(null);

		Iterator<AmazonProduct> actualIterator = spyProductIterator;
		assertTrue(actualIterator.hasNext());

		AmazonProduct retrievedProduct = actualIterator.next();

		assertTrue(retrievedProduct.getAsin().equals(asin1));

		AmazonProduct retrievedStation2 = actualIterator.next();
		assertTrue(retrievedStation2.getAsin().equals(asin1));
		assertTrue(actualIterator.hasNext());
		try {
			actualIterator.next();
		} catch (IllegalStateException e) {

		}
	}

}
