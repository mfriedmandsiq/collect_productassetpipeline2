package com.dsiq.amazonItemLookup;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class ZonProductFacade {
	// this handles interactions with the Amazon product collect table
	private static String phoenixDbUrl;

	private static final String ASIN_COL_NAME = "A";
	private static final String TITLE_COL_NAME = "T";
	private static final String B = "B";
	private static final String CA = "CA";
	private static final String DT = "DT";
	private static final String WC = "WC";
	private static final String RA = "RA";
	private static final String RE = "RE";
	private static final String LP = "LP";
	private static final String PR = "PR";
	private static final String DI = "DI";
	private static final String CO = "CO";
	private static final String SR = "SR";
	private static final String AV = "AV";
	private static final String TH = "TH";
	private static final String PU = "PU";
	private static final String FU = "FU";
	private static final String SH = "SH";
	private static final String TP = "TP";
	private static final String PA = "PA";
	private static final String I1 = "I1";
	private static final String I2 = "I2";
	private static final String I3 = "I3";
	private static final String I4 = "I4";
	private static final String I5 = "I5";
	private static final String I6 = "I6";
	private static final String I7 = "I7";
	private static final String I8 = "I8";
	private static final String I9 = "I9";
	private static final String I10 = "I10";
	private static final String LTIME_COL_NAME = "LTIME";
	private static final String EXPTIME_COL_NAME = "EXPTIME";
	private static final String LSTATUS_COL_NAME = "LSTATUS";
	private static final String ACTIVE_COL_NAME = "ACTIVE";
	private static final String TABLE = "COLLECT_PRODUCT.AMZN_ZON";

	public ZonProductFacade(String phoenixDbUrl) {
		ZonProductFacade.phoenixDbUrl = phoenixDbUrl;
		if (phoenixDbUrl == null) {
			throw new NullPointerException("phoenixDbUrl must not be null");
		}
	}

	public static String getPhoenixDbUrl() {
		return phoenixDbUrl;
	}

	public static Connection getConnection() throws SQLException {
		try {
			Class.forName("org.apache.phoenix.jdbc.PhoenixDriver");
		} catch (ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
		return DriverManager.getConnection(getPhoenixDbUrl());
	}

	// return the first active, expired product, or null
	// includes all 33 fields 
	static final String NEXT_PRODUCT_QUERY = String
			.format("SELECT %s, %s, %s, %s, %s," // 5
					+ "%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s,\n"  // 21
					+ "%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, \n"  // + 11 more = 31
					+ "TO_CHAR(COALESCE(TO_DATE(%s), TO_DATE('2000-01-01 00:00:00'))-current_time()) as DIFF FROM %s\n"
					+ "WHERE (current_date()>COALESCE(TO_DATE(%s), TO_DATE('2000-01-01 00:00:00'))) AND (%s = '1' OR ACTIVE IS NULL)\n"
					+ "ORDER BY %s, %s\n" + "LIMIT 1",
					ASIN_COL_NAME, TITLE_COL_NAME, B, // A, T , B // 3
					LSTATUS_COL_NAME, EXPTIME_COL_NAME, // LSTATUS, EXPTIME // 5
					CA, DT, WC, RA, RE, LP, PR, DI, CO, SR, AV, TH, PU, FU, SH, TP, PA, // 22
					I1, I2, I3, I4, I5, I6, I7, I8, I9, I10, LTIME_COL_NAME,   // 33
					EXPTIME_COL_NAME, TABLE, EXPTIME_COL_NAME,
					ACTIVE_COL_NAME,  // ACTIVE = 36
					EXPTIME_COL_NAME, ASIN_COL_NAME);

	static final String UPSERT_STATE_CHANGE = String.format(
			"UPSERT INTO %s(%s,%s,%s,%s) VALUES(?,?,?,?)", TABLE,
			ASIN_COL_NAME, LSTATUS_COL_NAME, LTIME_COL_NAME, EXPTIME_COL_NAME);

	public ProductData reserve() throws SQLException {
		System.out.println("In reserve()");
		Connection conn = null;
		try {
			conn = getConnection();
			System.out.println("**********************In reserve(), back from getConnection()**********");
			if (conn==null) {
				System.out.println("**********************In reserve(), conn is null**********");
			}
			System.out.println("NEXT_PRODUCT_QUERY = " + NEXT_PRODUCT_QUERY);
			ResultSet rset = conn.createStatement().executeQuery(
					NEXT_PRODUCT_QUERY);

			System.out.println("**********************back from executeQuery**********");
			System.out.println(String.format("Finding next product"));
			if (rset.next() && rset.getString(ASIN_COL_NAME) != null) {
				ProductData nextProduct = new ProductData();
				String A_VAL = rset.getString(ASIN_COL_NAME);
				String T_VAL = rset.getString(TITLE_COL_NAME);
				System.out.println(String.format("Found product %s, reserving.",
						A_VAL));
				String B_VAL = rset.getString(B);
				String CA_VAL = rset.getString(CA);
				String DT_VAL = rset.getString(DT);
				String WC_VAL = rset.getString(WC);
				String RA_VAL = rset.getString(RA);
				String RE_VAL = rset.getString(RE);
				String LP_VAL = rset.getString(LP);
				String PR_VAL = rset.getString(PR);
				String DI_VAL = rset.getString(DI);
				String CO_VAL = rset.getString(CO);
				String SR_VAL = rset.getString(SR);
				String AV_VAL = rset.getString(AV);
				String TH_VAL = rset.getString(TH);
				String PU_VAL = rset.getString(PU);
				String FU_VAL = rset.getString(FU);
				String SH_VAL = rset.getString(SH);
				String TP_VAL = rset.getString(TP);
				String PA_VAL = rset.getString(PA);
				String I1_VAL = rset.getString(I1);
				String I2_VAL = rset.getString(I2);
				String I3_VAL = rset.getString(I3);
				String I4_VAL = rset.getString(I4);
				String I5_VAL = rset.getString(I5);
				String I6_VAL = rset.getString(I6);
				String I7_VAL = rset.getString(I7);
				String I8_VAL = rset.getString(I8);
				String I9_VAL = rset.getString(I9);
				String I10_VAL = rset.getString(I10);
				nextProduct.addZonFieldVals(A_VAL, T_VAL, B_VAL, DT_VAL, CA_VAL, WC_VAL, RA_VAL, RE_VAL, LP_VAL, PR_VAL, DI_VAL, CO_VAL, SR_VAL, AV_VAL, TH_VAL, PU_VAL, FU_VAL, SH_VAL, TP_VAL, PA_VAL, I1_VAL, I2_VAL, I3_VAL, I4_VAL, I5_VAL, I6_VAL, I7_VAL, I8_VAL, I9_VAL, I10_VAL);
				updateState(conn, nextProduct, "RESERVED", Calendar.MINUTE, 10);
				return nextProduct;
			}
		} finally {
			if (conn != null) {
				conn.close();
			}
		}
		return null;
	}

	public boolean process(ProductData zonProduct) throws SQLException {
		System.out.println("In facade process()");
		if (!AmazonItemLookupQueryRunner.ASIN_RUN) {
			Connection conn = null;
			try {
				conn = getConnection();
				System.out.println("In facade process() A v2");
				if (zonProduct==null) {
					System.out.println("In facade process()v33--zonProduct is null");
				}

				String status = zonProduct.getLoadStatus();
				System.out.println("In facade process() AAA v33");
				if (!("RESERVED".equals(status))) {
					System.out.println("In facade process() AAAA v33");
					System.out.println(String.format(
							"Product %s must be reserved before processing. State=%s",
							zonProduct.getAsin(),
							zonProduct.getLoadStatus()));
					System.out.println("In facade process() AAAAA v2");
					/*throw new IllegalStateException(

						String.format(
								"Product %s must be reserved before processing. State=%s",
								amazonProduct.getAsin(),
								amazonProduct.getLoadStatus()));*/
					return false;
				}
				System.out.println("In facade process() B v2");
				if (System.currentTimeMillis() > Date.parse(zonProduct.getExpirationDate())) {
					System.out.println("In facade process() BB v2");
					throw new IllegalStateException(String.format(
							"Product %s is past its expiration time.",
							zonProduct.getAsin()));
				}
				System.out.println("In facade process() C v2");
				System.out.println(String.format("Processing product %s",
						zonProduct.getAsin()));
				System.out.println("In facade process() D");
				updateState(conn, zonProduct, "PROCESSING", Calendar.HOUR, 1);
				System.out.println("In facade process() E");
				return true;
			} finally {
				if (conn != null) {
					System.out.println("In facade process() F v33");
					conn.close();
					System.out.println("In facade process() G v33");
				}
			}
		}
		return true;
	}

	public void complete(ProductData zonProduct) throws SQLException {
		System.out.println("In complete()");
		if (!AmazonItemLookupQueryRunner.ASIN_RUN) {
		Connection conn = null;

		try {
			conn = getConnection();
			if (!("PROCESSING".equals(zonProduct.getLoadStatus()))) {
				throw new IllegalStateException(
						String.format(
								"Product %s must be in processing before done. State=%s",
								zonProduct.getAsin(),
								zonProduct.getLoadStatus()));
			}
			if (System.currentTimeMillis() > Date.parse(zonProduct.getExpirationDate())) {
				throw new IllegalStateException(String.format(
						"Product %s is past its expiration time!",
						zonProduct.getAsin()));
			}
			System.out.println(String.format("Completing product %s",
					zonProduct.getAsin()));
			updateState(conn, zonProduct, "COMPLETE", Calendar.MONTH, 8);
		} finally {
			if (conn != null) {
				conn.close();
			}
		}
		}
	}


	public void error(ProductData zonProduct) throws SQLException {
		System.out.println("In error()");
		if (!AmazonItemLookupQueryRunner.ASIN_RUN) {
		Connection conn = null;
		try {
			conn = getConnection();
			if (!("PROCESSING".equals(zonProduct.getLoadStatus()))) {
				throw new IllegalStateException(
						String.format(
								"Product %s must be in processing to be changed to ERROR status. State=%s",
								zonProduct.getAsin(),
								zonProduct.getLoadStatus()));
			}
			System.out.println(String
					.format("Product %s has error, setting new expiration date to 30 minutes later.",
							zonProduct.getAsin()));
			updateState(conn, zonProduct, "ERROR", Calendar.MINUTE, 30);
		} finally {
			if (conn != null) {
				conn.close();
			}
		}
		}
	}

	private void updateState(Connection conn, ProductData zonProduct,
			String newState, int expirationInterval, int expirationAmount)
					throws SQLException {
		System.out.println("in updateState");
		if (!AmazonItemLookupQueryRunner.ASIN_RUN) {
		PreparedStatement stmt = null;

		try {
			String pattern = "yyyy-MM-dd HH:mm:ss";
			SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
			GregorianCalendar gregC = new GregorianCalendar();
			System.out.println("EXPTIME_COL_NAME = " + EXPTIME_COL_NAME);
			stmt = conn.prepareStatement(UPSERT_STATE_CHANGE);
			stmt.setString(1, zonProduct.getAsin());
			zonProduct.setLoadStatus(newState);
			stmt.setString(2, zonProduct.getLoadStatus());
			Date date = gregC.getTime();
			String loadDate = "";

			loadDate = dateFormat.format(date);

			System.out.println("*******************setting loadDate to " + loadDate);
			zonProduct.setLoadDate(loadDate);
			stmt.setString(3, loadDate);

			gregC.add(expirationInterval, expirationAmount);
			date = gregC.getTime();

			String expirationDate = "";
			expirationDate = dateFormat.format(date);

			System.out.println("*******************setting exp date to " + expirationDate);
			zonProduct.setExpirationDate(expirationDate);
			stmt.setString(4, expirationDate);
			stmt.execute();
			int updateCount = stmt.getUpdateCount();
			if (updateCount != 1) {
				throw new SQLException(
						String.format(
								"UpdateCount=%s, Unable to change product state, product = %s",
								updateCount, zonProduct.getAsin()));
			}
			conn.commit();
		} finally {
			if (stmt != null)
				stmt.close();
		}
	}
}

}