package com.dsiq.amazonItemLookup;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ASINProductIterator extends ProductIterator{
	
	public ASINProductIterator(String phoenixDbUrl) {
		super(phoenixDbUrl);
	}

	static final int CHUNK_SIZE = 10000;

	public static void main(String[] args) {
	}
	
    static long decode(final String value) {
		    return Long.parseLong(value, 36);
    }
		 
	static String encode(final long value) {
		    return Long.toString(value, 36);
	}
	
	BufferedReader br;
	List<String> lines;
	boolean done;
	int unprocessedLines;
	int currentLineNumber;
	int lineInFile;
	
	public ASINProductIterator(String filename, String phoenixDbUrl) {
		super(phoenixDbUrl);
		try {
			br = new BufferedReader(new FileReader(filename));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		lines = null;
		done = false;
		lineInFile = 0;
		getNextChunk();
	}
		
	private int getNextChunk() {
		String line = null;
		try {
			// skip over lines
			while (lineInFile < AmazonItemLookupQueryRunner.initialLine) {
			line = br.readLine();
			lineInFile++;
			System.out.println("**********lineInFile = " + lineInFile + "**********");
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		lines = new ArrayList<String>();
		int count = 0;
		while (line != null && count <= CHUNK_SIZE) {
		     lines.add(line);
		     try {
				line = br.readLine();
			} catch (IOException e) {
				e.printStackTrace();
			}
		     count++;
		}
		if (count == 0)
			done = true;
		currentLineNumber = 0;
		unprocessedLines = count;
		return count;
		// Random r = new Random();
		// String randomLine = lines.get(r.nextInt(lines.size()));
	}

	@Override
	public boolean hasNext() {
		System.out.println("In ASINIterator.hasNext()");
		return unprocessedLines > 0;
	}

	@Override
	public ProductData next() {
		System.out.println("In ASINProductIterator.next()");
		if (unprocessedLines <= 0)
			return null;
		String asin = lines.get(currentLineNumber);
		unprocessedLines--;
		currentLineNumber++;
		if (unprocessedLines==0)
			getNextChunk();
		ProductData productData = new ProductData();
		productData.setAsin(asin);
		return productData;
	}

	@Override
	public void remove() {
	}

}
