package com.dsiq.amazonItemLookup;

public class ProductData {
	public static final int ASIN_LENGTH = 10;
	
	private transient String A = null;
	private String UPC = null;
	private String loadStatus = null;
	private String expirationDate = null;
	private String loadDate = null;
	private String T = null;
	private String B = null;
	private String listedPrice = null;
	private String CA = null;
	private String DT = null;
	private String WC = null;
	private String RA = null;
	private String RE = null;
	private String LP = null;
	private String PR = null;
	private String DI = null;
	private String CO = null;
	private String SR = null;
	private String AV = null;
	private String TH = null;
	private String PU = null;
	private String FU = null;
	private String SH = null;
	private String TP = null;
	private String PA = null;
	private String I1 = null;
	private String I2 = null;
	private String I3 = null;
	private String I4 = null;
	private String I5 = null;
	private String I6 = null;
	private String I7 = null;
	private String I8 = null;
	private String I9 = null;
	private String I10 = null;
	// private transient String ACTIVE;
	// private transient String LSTATUS;
	// private transient String LTIME;
	// private transient String EXPTIME;
	
	public ProductData() {
	}
	
	public void addZonFieldVals (
			String A,
			String T,
			String B,
			String CA,
			String DT,
			String WC,
			String RA,
			String RE,
			String LP,
			String PR,
			String DI,
			String CO,
			String SR,
			String AV,
			String TH,
			String PU,
			String FU,
			String SH,
			String TP,
			String PA,
			String I1,
			String I2,
			String I3,
			String I4,
			String I5,
			String I6,
			String I7,
			String I8,
			String I9,
			String I10
			) {
		this.A = A;
		this.T = T;
		this.B = B;
		this.CA = CA;
		this.DT = DT;
		this.WC = WC;
		this.RA = RA;
		this.RE = RE;
		this.LP = LP;
		this.PR = PR;
		this.DI = DI;
		this.CO = CO;
		this.SR = SR;
		this.AV = AV;
		this.TH = TH;
	    this.PU = PU;
		this.FU = FU;
		this.SH = SH;
		this.TP = TP;
		this.PA = PA;
		this.I1 = I1;
		this.I2 = I2;
		this.I3 = I3;
		this.I4 = I4;
		this.I5 = I5;
		this.I6 = I6;
		this.I7 = I7;
		this.I8 = I8;
		this.I9 = I9;
		this.I10 = I10;
	}
	
		
	public String getAsin() {
		return A;
	}
	public void setAsin(String asin) {
		this.A = asin;
	}
	public String getUPC() {
		return UPC;
	}
	public void setUPC(String UPC) {
		this.UPC = UPC;
	}
	public String getLoadStatus() {	
		return loadStatus;
	}
	public void setLoadStatus(String loadStatus) {
		this.loadStatus = loadStatus;
	}
	public String getExpirationDate() {
		return expirationDate;
	}
	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}
	public String getLoadDate() {
		return loadDate;
	}
	public void setLoadDate(String loadDate) {
		this.loadDate = loadDate;
	}
	public String getTitle() {
		return T;
	}
	public void setTitle(String title) {
		this.T = title;
	}
	public String getListedPrice() {
		return listedPrice;
	}
	public void setListedPrice(String listedPrice) {
		this.listedPrice = listedPrice;
	}
	
	public String getBrand() {
		return B;
	}
	public String getCategory() {
		return CA;
	}
	public String getDescription() {
		return DT;
	}
	public String getWordCount() {
		return WC;
	}
	public String getRating() {
		return RA;
	}
	public String getReview() {
		return RE;
	}
	public String getListPrice() {
		return LP;
	}
	public String getPrice() {
		return PR;
	}
	public String getDiscount() {
		return DI;
	}
	public String getCommission() {
		return CO; 
	}
	public String getSalesRank() {
		return SR;
	}
	public String getAvailable() {
		return AV;
	}
	public String getThumbnail() {
		return TH;
	}
	public String getProductURL() {
		return PU;
	}
	public String getFulfilled() {
		return FU;		
	}
	public String Shipping() {
		return SH;
	}
	public String TotalPrice() {
		return TP;
	}
	public String getParent() {
		return PA;
	}
}
