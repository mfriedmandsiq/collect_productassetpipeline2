package com.dsiq.amazonItemLookup;

import java.net.HttpURLConnection;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.BasicConfigurator;

import com.dsiq.amazonItemLookup.AmazonApiToStaging;
import com.dsiq.amazonItemLookup.ProductData;
import com.dsiq.amazonItemLookup.ZonProductIterator;

public class AmazonItemLookupQueryRunner {

	private static final int SECONDS = 1000; // in ms
	private static final int MINUTES = 60 * SECONDS; // in ms

	private String phoenixDbUrl = null;

	private HttpURLConnection conn;
	private ZonProductIterator zonProductIterator;
	private ASINProductIterator asinProductIterator;
	private AmazonApiToStaging amazonApiToStaging;

	static boolean launchedByMain = false;

	public static boolean ASIN_RUN;
	public static long initialLine = 0;

	public static void main (String[] args) {
		launchedByMain = true;
		ASIN_RUN = (args.length > 1 && args[1].equals("ASIN_RUN"));
		if (args.length > 2) {
			initialLine = Long.parseLong(args[2]);
		}
		System.out.println("ASIN_RUN = " + ASIN_RUN);
		BasicConfigurator.configure();
		AmazonItemLookupQueryRunner runner = new AmazonItemLookupQueryRunner();
		runner.setPhoenixDbUrl(args[0]);
		runner.run();
	}

	public AmazonItemLookupQueryRunner() {
		amazonApiToStaging = new AmazonApiToStaging();
	}

	public synchronized void shutdown() {
		conn.disconnect();
	}

	public void setPhoenixDbUrl(String phoenixDbUrl) {
		this.phoenixDbUrl = phoenixDbUrl;
	}


	public void run() {
		if (ASIN_RUN) {
			setASINProductIterator(new ASINProductIterator("SortedUniqueASINs", phoenixDbUrl));
			runIterator(asinProductIterator);
		}
		else {
			setZonProductIterator(new ZonProductIterator(phoenixDbUrl));
			runIterator(zonProductIterator);
		}

	}

	public ZonProductIterator getAmazonProductIterator() {
		return zonProductIterator;
	}


	public ASINProductIterator getASINProductIterator() {
		return asinProductIterator;
	}

	public void setZonProductIterator(
			ZonProductIterator zonProductIterator) {
		this.zonProductIterator = zonProductIterator;
	}

	public void setASINProductIterator(
			ASINProductIterator asinProductIterator) {
		this.asinProductIterator = asinProductIterator;
	}

	void runIterator(ProductIterator iterator) {
		while (iterator.hasNext()) {
			ProductData currentProduct = iterator.next();
			if (currentProduct==null) {
				System.out.println("******************currentProduct is null**********************");
				if (iterator instanceof ASINProductIterator)
				{
					iterator = new ASINProductIterator("SortedUniqueASINs", phoenixDbUrl);
					setASINProductIterator((ASINProductIterator)iterator);
					continue;
				}
				try {
					Thread.sleep(5 * MINUTES);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			try {
				if (!AmazonItemLookupQueryRunner.ASIN_RUN)
					iterator.process(currentProduct);
				try {
					processProduct(currentProduct);
				} catch (RuntimeException re) {
					if (!AmazonItemLookupQueryRunner.ASIN_RUN)
						iterator.error(currentProduct);
					System.err.println(String
							.format("Exception while processing product %s (process failed)\n, %s.",
									currentProduct.getAsin(),
									ExceptionUtils.getStackTrace(re)));
				}
				try {
					if (!AmazonItemLookupQueryRunner.ASIN_RUN)
						iterator.complete(currentProduct);

				} catch (RuntimeException re) {
					if (!AmazonItemLookupQueryRunner.ASIN_RUN)
						iterator.error(currentProduct);
					System.err.println(String
							.format("Exception while processing product %s (fail to complete).\n%s",
									currentProduct.getAsin(),
									ExceptionUtils.getStackTrace(re)));
				}

				try {
					System.out.println("sleeping 1.5 seconds");
					// Thread.sleep(5 * SECONDS);
					Thread.sleep(500);
				} catch (InterruptedException e) {
					System.out.println("Warning - " + e.getMessage());
				}
			} catch (Throwable t) {
				System.out.println(String.format("processException\n%s",
						ExceptionUtils.getStackTrace(t)));
				// re-throw all Errors
				if (t instanceof Error) {
					throw (Error) t;
				}
			}
		}
	}

	public AmazonApiToStaging getAmazonApiToStaging() {
		return amazonApiToStaging;
	}

	void processProduct(ProductData product) {
		System.out.println((String.format("process product asin = %s",
				product.getAsin())));
		try {
			getAmazonApiToStaging()
			.getProductContent(product);
		} catch (Exception ex) {
			System.out.println("Warning - Exception: "	+ ex.getMessage());
		}
	}
}