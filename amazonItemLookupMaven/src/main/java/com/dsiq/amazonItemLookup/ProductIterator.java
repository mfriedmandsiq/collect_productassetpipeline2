package com.dsiq.amazonItemLookup;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ProductIterator implements Iterator<ProductData> {
	final ZonProductFacade zonProductFacade;
	String phoenixDbUrl = null;
	List<String> zonProducts = null;
	Iterator<ProductData> zonProductIterator = null;
	ProductData nextZonProduct = null;

	public ProductIterator(String phoenixDbUrl) {
		this.phoenixDbUrl = phoenixDbUrl;
		if (phoenixDbUrl == null) {
			throw new NullPointerException("phoenixDbUrl must not be null");
		}
		zonProductFacade = new ZonProductFacade(phoenixDbUrl);
		if (!AmazonItemLookupQueryRunner.ASIN_RUN)
		    getNextZonProduct();
	}

	public ZonProductFacade getZonProductFacade() {
		return zonProductFacade;
	}

	public void setAmazonProductIterator(
			Iterator<ProductData> zonProductIterator) {
		this.zonProductIterator = zonProductIterator;
	}

	public void process(ProductData zonProduct) {
		if (zonProduct==null) {
    		System.out.println("in ZonProductIterator.process(), zonProduct is null************");
		}
		try {
			zonProductFacade.process(zonProduct);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public void complete(ProductData amazonProduct) {
		System.out.println("in complete");
		try {
			zonProductFacade.complete(amazonProduct);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
	
	public void error(ProductData zonProduct) {
		System.out.println("in error");
		try {
			zonProductFacade.error(zonProduct);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	void setNextZonProduct(ProductData nextZonProduct) {
		System.out.println("in setNextAmazonProduct");
		if (nextZonProduct==null) {
    		System.out.println("in setNextZonProduct, setting to null************");
		}
		this.nextZonProduct = nextZonProduct;
	}

	ProductData getNextZonProduct() {
		System.out.println("in getNextZonProduct() v2");
		try {
			setNextZonProduct(getZonProductFacade().reserve());
			System.out.println("in getNextAmazonProduct() v2 A");
			if (nextZonProduct != null) {
				System.out.println(String.format(
						"NextAmazonProduct is loaded into cache %s",
						nextZonProduct.getAsin()));
			}
			else {
				System.out.println("in getNextAmazonProduct, nextAmazonProduct is null");
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return nextZonProduct;
	}

	// @Override
	public void remove() {
		System.out.println("in remove()");
		throw new UnsupportedOperationException(
				"AmazonProducts are read-only collections.");
	}

	public boolean hasNext() {
		System.out.println("in hasNext() v2");
		getNextZonProduct();
		System.out.println("in hasNext() v3333333  A");
		String pattern = "yyyy-MM-dd HH:mm:ss";
		SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
		try {
			if (nextZonProduct != null
					&& System.currentTimeMillis() < (dateFormat.parse(nextZonProduct
							.getExpirationDate())).getTime()
							) {
				System.out.println(String.format("Next v3333333 product is reserved %s",
							nextZonProduct.getAsin()));
				return true;
			} else {
				// discard the expired next lat-lon
				System.out.println("***************in hasNext() setting next product to null*******");
				setNextZonProduct(null);
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return false;
	}

	public ProductData next() {
		System.out.println("in next() v2");
		if (nextZonProduct==null) {
			System.out.println("***nextAmazonProduct is null***");
		}
		System.out.println("in runAmazonProductIterator v2 A");
		return nextZonProduct;
	}
}
