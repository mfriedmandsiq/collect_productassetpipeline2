package com.dsiq.amazonItemLookup.JSON;

import java.io.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class XMLExtractor {

	private static final Logger logger = LoggerFactory
			.getLogger(XMLExtractor.class);

	private XPath xpath;
	private Document document;

	public XMLExtractor(String xml) {
		
		xml = xml.replace(" xmlns=\"http://webservices.amazon.com/AWSECommerceService/2011-08-01\"","");

		InputSource source = new InputSource(new StringReader(
			    xml));
		DocumentBuilderFactory dbf = null;
		DocumentBuilder db = null;
		document = null;

		try {
			dbf = DocumentBuilderFactory.newInstance();
			db = dbf.newDocumentBuilder();
			document = db.parse(source);
			xpath =  XPathFactory.newInstance().newXPath();
		} catch (ParserConfigurationException e) {
			logger.warn(e.getMessage());
		} catch (SAXException e) {
			logger.warn(e.getMessage());
		} catch (IOException e) {
			logger.warn(e.getMessage());
		}

	}

	public String extract(String fieldname, String path) {
		String str = "";
		System.out.println("Extracting " + fieldname + " using xPath " + path);
		try {
			str = xpath.compile(path).evaluate(document);
		} catch (XPathExpressionException e) {
			logger.warn("Error in xpath expression: " + e.getMessage());
		}

		return str;
	}

}
