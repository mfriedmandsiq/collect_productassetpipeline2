package com.dsiq.amazonItemLookup;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.text.DecimalFormat;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import com.dsiq.amazonItemLookup.JSON.XMLExtractor;
import com.google.common.base.Strings;

public class AmazonApiToStaging {

	static String[] stagingFieldNames = { "dummy", // 0
		"A", // 1
		"UPC", // 2
		"SKU", // 3
		"T", // 4
		"CA", // 5
		"SC", // 6
		"DP", // 7
		"GL", // 8
		"AR", // 9
		"NR", // 10
		"H", // 11
		"L", // 12
		"W", // 13
		"WT", // 14
		"AVT", // 15
		"B", // 16
		"PH", // 17
		"PL", // 18
		"PW", // 19
		"WG", // 20 -
		"PQ", // 21
		"SZ", // 22 -
		"CL", // 23
		"CP", // 24
		"LCP", // 25
		"LP", // 26
		"LRP", // 27
		"LUP", // 28
		"SR", // 29
		"PN", // 30
		"PU", // 31
		"ST", // 32
		"MF", // 33
		"MDL", // 34
		"MPN", // 35
		"PD", // 36
		"TS", // 37
		"X" // 38
	};

	final static String NOT_PRESENT = "NOT_PRESENT";

	final static String[] xmlPath = {
		"", // (dummy) 0
		"/ItemLookupResponse/Items/Item/asin",// A 1
		"/ItemLookupResponse/Items/Item/ItemAttributes/EAN",// UPC 2
		"/ItemLookupResponse/Items/Item/ItemAttributes/SKU", // SKU 3
		"/ItemLookupResponse/Items/Item/ItemAttributes/Title", // T 4
		"/ItemLookupResponse/Items/Item/BrowseNodes/BroseNode/Ancestors/BrowseNode/Ancestors/BrowseNode/Ancestors/BrowseNode/Name",// CA 5
		"/ItemLookupResponse/Items/Item/BrowseNodes/BrowseNode/Ancestors/BrowseNode/Ancestors/BrowseNode/Name",// SC 6
		"/ItemLookupResponse/Items/Item/ItemAttributes/Title", // DP 7
		"/ItemLookupResponse/Items/Item/BrowseNodes/BrowseNode/Name", // GL 8
		"NOT_PRESENT", // AR (average review rating) 9
		"NOT_PRESENT", // NR (number of reviews) 10
		"/ItemLookupResponse/Items/Item/ItemAttributes/ItemDimensions/Height", // H 11
		"/ItemLookupResponse/Items/Item/ItemAttributes/ItemDimensions/Length", // L 12
		"/ItemLookupResponse/Items/Item/ItemAttributes/ItemDimensions/Width", // W 13
		"/ItemLookupResponse/Items/Item/ItemAttributes/ItemDimensions/Weight/content", // WT 14
		"/ItemLookupResponse/Items/Item/Offers/Offer/OfferListing/AvailabilityAttributes/AvailabilityType", // AVT 15
		"/ItemLookupResponse/Items/Item/ItemAttributes/Brand", // B 16
		"/ItemLookupResponse/Items/Item/ItemAttributes/PackageDimensions/Height/content", // PH 17
		"/ItemLookupResponse/Items/Item/ItemAttributes/PackageDimensions/Length/content", // PL 18
		"/ItemLookupResponse/Items/Item/ItemAttributes/PackageDimensions/Width/content", // PW 19
		"/ItemLookupResponse/Items/Item/ItemAttributes/PackageDimensions/Weight/content", // WG 20
		"/ItemLookupResponse/Items/Item/ItemAttributes/PackageQuantity", // PQ 21
		"NOT_PRESENT", // SZ (size) 22
		"/ItemLookupResponse/Items/Item/ItemAttributes/Color", // CL 23
		"/ItemLookupResponse/Items/Item/Offers/Offer/Price/FormattedPricet", // CP 24
		"/ItemLookupResponse/Items/Item/OfferSummary/LowestNewPrice/FormattedPrice", // LCP
		// 25
		"/ItemLookupResponse/Items/Item/ItemAttributes/ListPrice/FormattedPrice", // LP
		// 26
		"/ItemLookupResponse/Items/Item/OfferSummary/LowestRefurbishedPrice/FormattedPrice",
		// 27
		"/ItemLookupResponse/Items/Item/OfferSummary/LowestUsedPrice/FormattedPrice", // LUP
		// 28
		"/ItemLookupResponse/Items/Item/Offers/Offer/SalesRank", // SR 29
		"/ItemLookupResponse/Items/Item/ItemAttributes/PartNumber", // PN 30
		"/ItemLookupResponse/Items/Item/ItemAttributes/Publisher", // PU 31
		"/ItemLookupResponse/Items/Item/ItemAttributes/Studio", // ST 32
		"/ItemLookupResponse/Items/Item/ItemAttributes/Manufacturer", // MF 33
		"/ItemLookupResponse/Items/Item/ItemAttributes/Model", // MDL 34
		"/ItemLookupResponse/Items/Item/ItemAttributes/MPN", // MPN 35
		"/ItemLookupResponse/Items/Item/ItemAttributes/Feature", // PD 36
		"/ItemLookupResponse/Items/Item/timestamp", // TS 37
		"NOT_PRESENT" // X 38
	};

	final static String WTU_Xpath = "/ItemLookupResponse/Items/Item/ItemAttributes/ItemDimensions/Weight/Units";
	final static String PHU_Xpath = "/ItemLookupResponse/Items/Item/ItemAttributes/PackageDimensions/Height/Units";
	final static String PLU_Xpath = "/ItemLookupResponse/Items/Item/ItemAttributes/PackageDimensions/Length/Units";
	final static String PWU_Xpath = "/ItemLookupResponse/Items/Item/ItemAttributes/PackageDimensions/Width/Units";
	final static String WGU_Xpath = "/ItemLookupResponse/Items/Item/ItemAttributes/PackageDimensions/Weight/Units";

	enum dbTypes {
		NONE, VARCHAR, UNSIGNED_INT, UNSIGNED_FLOAT, UNSIGNED_DOUBLE, TIME
	};

	static dbTypes[] dbFieldType = {

		dbTypes.NONE, // (dummy) 0
		dbTypes.VARCHAR, // A 1
		dbTypes.VARCHAR, // UPC 2
		dbTypes.VARCHAR, // SKU 3
		dbTypes.VARCHAR, // T 4
		dbTypes.VARCHAR,// CA 5
		dbTypes.VARCHAR,// SC 6
		dbTypes.VARCHAR, // DP 7
		dbTypes.VARCHAR, // GL 8
		dbTypes.UNSIGNED_DOUBLE, // AR (average review rating) 9
		dbTypes.UNSIGNED_INT, // NR (number of reviews) 10
		dbTypes.VARCHAR, // H 11
		dbTypes.VARCHAR, // L 12
		dbTypes.VARCHAR, // W 13
		dbTypes.VARCHAR, // WT 14
		dbTypes.VARCHAR, // AVT 15
		dbTypes.VARCHAR, // B 16
		dbTypes.VARCHAR, // PH 17
		dbTypes.VARCHAR, // PL 18
		dbTypes.VARCHAR, // PW 19
		dbTypes.VARCHAR, // WG 20
		dbTypes.VARCHAR, // PQ 21
		dbTypes.VARCHAR, // SZ (size) 22
		dbTypes.VARCHAR, // CL 23
		dbTypes.VARCHAR, // CP 24
		dbTypes.VARCHAR, // LCP 25
		dbTypes.VARCHAR, // LP 26
		dbTypes.VARCHAR, // LRP 27
		dbTypes.VARCHAR, // LUP 28
		dbTypes.VARCHAR, // SR 29
		dbTypes.VARCHAR, // PN 30
		dbTypes.VARCHAR, // PU 31
		dbTypes.VARCHAR, // ST 32
		dbTypes.VARCHAR, // MF 33
		dbTypes.VARCHAR, // MDL 34
		dbTypes.VARCHAR, // MPN 35
		dbTypes.VARCHAR, // PD 36
		dbTypes.VARCHAR, // TS 37
		dbTypes.VARCHAR //  X 38
	};
	
	final int XML_FIELD = 38;

	public void getProductContent(ProductData product) {
		System.out.println("in getAmazonProductContent");
		String sURL = createURL(product);
		System.out.println("sURL(str) = " + sURL);

		HttpGet httpget = new HttpGet(sURL);
		String responseXml;
		int times = 0;
		do {
			times++;
			responseXml = null;
			try {
				HttpClient httpclient = new DefaultHttpClient();
				System.out.println("Calling httpclient.execute(httpget, getResponseHandler())");
				responseXml = httpclient.execute(httpget, getResponseHandler());
				System.out.println(String.format("Response received\n%s", responseXml));
				if (responseXml.contains("not a valid value for ItemId"))
					return;
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			if (times > 1) {
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		} while (responseXml == null && times++ < 5);

		if (Strings.isNullOrEmpty(responseXml))
			return;

		writeToStaging(responseXml, product);

	}

	public String createURL(ProductData product) {
		return AmazonItemLookup.ItemLookupURL(product.getAsin());
	}

	@SuppressWarnings("unused")
	private void setHttpGetHeaders(HttpGet httpget) {
		httpget.setHeader("Host", "ecs.amazonaws.com");
		httpget.setHeader("Connection", "keep-alive");
		httpget.setHeader("Cache-Control", "max-age=0");
		httpget.setHeader(
				"User-Agent",
				"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36");
		httpget.setHeader("Accept",
				"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
		httpget.setHeader("DNT", "1");
		httpget.setHeader("Accept-Encoding", "gzip,deflate,sdch");
		httpget.setHeader("Accept-Language", "en-US,en;q=0.8");
	}

	public ResponseHandler<String> getResponseHandler() {
		ResponseHandler<String> responseHandler = new ResponseHandler<String>() {
			public String handleResponse(final HttpResponse response)
					throws ClientProtocolException, IOException {
				int status = response.getStatusLine().getStatusCode();
				if (status >= 200 && status < 300) {
					HttpEntity entity = response.getEntity();
					return entity != null ? EntityUtils.toString(entity) : null;
				} else {
					throw new ClientProtocolException(
							"Unexpected response status: " + status);
				}
			}
		};
		return responseHandler;
	}

    /**
     * 
     * On an ASIN run, we do not want to wipe out rating and review metrics,
     * so we check for this condition so we can omit from the upsert
     * 
     * @param i   The field index
     * @return true if the field is to be omitted on an ASIN run
     */
    boolean omit(int i) {
		return(AmazonItemLookupQueryRunner.ASIN_RUN & stagingFieldNames[i].equals("AR") ||
				stagingFieldNames[i].equals("NR"));
    }

	void writeToStaging(String responseXml, ProductData product) {
		XMLExtractor extractor = new XMLExtractor(responseXml);
		PreparedStatement stmt = null;
		String wt_units = extractor.extract("WTU", WTU_Xpath);
		String ph_units = extractor.extract("PHU", PHU_Xpath);
		String pl_units = extractor.extract("PLU", PLU_Xpath);
		String pw_units = extractor.extract("PWU", PWU_Xpath);
		String wg_units = extractor.extract("WGU", WGU_Xpath);

		String val = null;
		String v1 = null;
		int span = AmazonApiToStaging.stagingFieldNames.length;

		try {
			Connection conn = null;
			conn = ZonProductFacade.getConnection();
			conn.setAutoCommit(true);
			StringBuilder sbQFields = new StringBuilder();
			StringBuilder sbQValues = new StringBuilder();
			StringBuilder sb = new StringBuilder();
			sb.append("UPSERT INTO STAGING_PRODUCT.AMZN (");
			String delimiter = "";

			for (int i = 1; i < span; i++) {
				if (omit(i))
					continue;
				sbQFields.append(delimiter);
				sbQFields.append(stagingFieldNames[i]);
				delimiter = ",";
			}
			delimiter = "";

			for (int i = 1; i < span; i++) {
				if (omit(i))
					continue;
				sbQValues.append(delimiter);
				sbQValues.append("?");
				delimiter = ",";
			}
			sb.append(sbQFields);
			sb.append(") VALUES (");
			sb.append(sbQValues);
			sb.append(")");

			stmt = conn.prepareStatement(sb.toString());


			int pos = 1; // the position in the prepared statement to set; can trail i in the general case
			stmt.setString(pos, product.getAsin());
			for (int i = 2; i < span; i++) {
				if (omit(i))
					continue;
				pos++;
				System.out.println("i = " + i + ", XML_FIELD = " + XML_FIELD);
				if (AmazonApiToStaging.xmlPath[i].equals(AmazonApiToStaging.NOT_PRESENT)) {
					System.out.println("Found NOT_PRESENT");
					if (i==XML_FIELD) {
						System.out.println("setting XML to " + responseXml);
						System.out.println("pos = " + responseXml);
						stmt.setString(pos, responseXml);
						continue;
					} else {
					val = null;
					}
				} else {
					v1 = extractor.extract(stagingFieldNames[i], xmlPath[i]);
					// add units if applicable; also other special cases
					
					switch (stagingFieldNames[i]) {
					case "WT":
						if (Strings.isNullOrEmpty(v1))
							val = v1;
						else
						    val = v1 + " " + wt_units;
						break;
						
					case "PH":
						if (Strings.isNullOrEmpty(v1))
							val = v1;
						else
						    val = v1 + " " + ph_units;
						break;
						
					case "PL":
						if (Strings.isNullOrEmpty(v1))
							val = v1;
						else
						    val = v1 + " " + pl_units;
						break;
					case "PW":
						if (Strings.isNullOrEmpty(v1))
							val = v1;
						else
						    val = v1 + " " + pw_units;
						break;
					case "WG":
						if (Strings.isNullOrEmpty(v1))
							val = v1;
						else
						    val = v1 + " " + wg_units;
						break;
					case "AR":
						val = String.valueOf(product.getRating());
					case "NR":
						val = String.valueOf(product.getReview());
					case "UPC":
						String upc = product.getUPC();
						if (!Strings.isNullOrEmpty(upc)) {
							val = upc;
						}
						else {
							val = v1;
						}
					case "B":
						String b = product.getBrand();
						if (!Strings.isNullOrEmpty(b)) {
							val = b;
						}
						else {
							val = v1;
						}
						
					default:
						val = v1;
					}
				}
				dbTypes type = dbFieldType[i];

				System.out.println("val = " + val + ", type = " + type.toString());
				// /ItemLookupResponse/Items/Item/timestamp
				// replace corresponding placeholder with value
				try {
				switch (type) {
				case VARCHAR:
					if (val == null)
						stmt.setNull(pos, Types.VARCHAR);
					stmt.setString(pos, val);
					break;
				case UNSIGNED_INT:
					if (Strings.isNullOrEmpty(val)) {
						stmt.setNull(pos, Types.INTEGER);
					} else {
						stmt.setInt(pos, Math.abs(Integer.parseInt(val)));
					}
					break;
				case UNSIGNED_FLOAT:
					if (Strings.isNullOrEmpty(val)) {
						stmt.setNull(pos, Types.FLOAT);
					} else {
						stmt.setFloat(pos, Math.abs(Float.parseFloat(val)));
					}
					break;
				case UNSIGNED_DOUBLE:
					if (Strings.isNullOrEmpty(val)) {
						stmt.setNull(pos, Types.DOUBLE);
					} else {
						stmt.setDouble(pos, Math.abs(Double.parseDouble(val)));
					}
					break;
				default:
					if (val == null)
						stmt.setNull(pos, Types.VARCHAR);
					stmt.setString(pos, val);
					break;
				}
				} catch(Exception e) {
					System.out.println("Exception in setting: " + e.getMessage());
					
				}
			}
			System.out.println("about to call execute HBase");
			stmt.execute();
			System.out.println("back from execute");
			stmt.close();
		} catch (SQLException ex) {
			System.err.println("SQL Exception: " + ex.getMessage());
		} catch (Exception ex) {
			System.err.println("Exception: " + ex.getMessage());
			System.err.println(ex.getStackTrace());
		}
	}
	
	String dollarize(String amt) {
		if (Strings.isNullOrEmpty(amt)) {
			return "";
		}
		Double amtD = Double.valueOf(amt);
		amtD = amtD / 100.0;
		DecimalFormat df = new DecimalFormat("#.##");
		String str = df.format(amtD);
		return str;
	}
}
