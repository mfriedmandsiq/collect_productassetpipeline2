package com.dsiq.amazonItemLookup;

import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

/*
 * For sample code, see 
 * https://aws.amazon.com/code/Product-Advertising-API/2478
 * 
 * For product ad api documentation, see
 * See http://docs.aws.amazon.com/AWSECommerceService/latest/DG/ItemLookup.html
 */
public class AmazonItemLookup {
	public static final String AWS_ACCESS_KEY_ID = "AKIAITFKI4IML4NPSHNA";
	public static final String AWS_SECRET_KEY = "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";
	public static final String ENDPOINT = "ecs.amazonaws.com";
	// public static final String ENDPOINT = "72.21.194.213";

	public static final String SERVICE = "AWSECommerceService";
	public static final String AMAZON_VERSION = "2011-08-01";
	public static final String OPERATION = "ItemLookup";
	public static final String RESPONSE_GROUP = ResponseGroup.Large.toString();
	public static final String TAG = "mytag-20";

	public static enum ResponseGroup {
		Accessories, BrowseNodes, CustomerReviews, EditorialReview, Images, ItemAttributes, ItemIds, Large, Medium, OfferFull, Offers, PromotionSummary, OfferSummary, RelatedItems, Reviews, SalesRank, Similarities, Small, Tracks, VariationImages, Variations, VariationSummary
	}

	/**
	 * Given an ASIN, generate a complete URL
	 * 
	 * @param item_id
	 * @return
	 */
	public static String ItemLookupURL(String item_asin_id) {
		SignedRequestsHelper helper;
		try {
			helper = SignedRequestsHelper.getInstance(ENDPOINT,
					AWS_ACCESS_KEY_ID, AWS_SECRET_KEY);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

		String requestUrl = null;

		Map<String, String> params = new HashMap<String, String>();
		params.put("Service", SERVICE);
		params.put("Version", AMAZON_VERSION);
		params.put("Operation", OPERATION);
		params.put("ItemId", item_asin_id);
		params.put("ResponseGroup", RESPONSE_GROUP);
		params.put("AssociateTag", TAG);
		requestUrl = helper.sign(params);

		// params.put("Service", "AWSECommerceService");
		// params.put("Operation", "ItemLookup");
		// params.put("ItemId", item_asin_id);
		// params.put("AssociateTag", "mytag-20");

		return requestUrl;
	}

	public static String fetch(String requestUrl, String tagName, int index) {
		String title = null;
		for (int attempt = 1; attempt <= 10; attempt++) {
			try {
		//		System.out.println("in fetch v2");
				DocumentBuilderFactory dbf = DocumentBuilderFactory
						.newInstance();
				DocumentBuilder db = dbf.newDocumentBuilder();
				Document doc = db.parse(requestUrl);
				Node titleNode = doc.getElementsByTagName(tagName).item(index);
				if (titleNode != null) {
					title = titleNode.getTextContent();
					break;
				}
			} catch (Exception e) {
				int id = requestUrl.indexOf("ItemId");
				System.err.println(String.format(
						"Final attempt failed for asin %s",
						requestUrl.substring(id + 7, id + 17)));
			}
		}
		return title;
	}
}
