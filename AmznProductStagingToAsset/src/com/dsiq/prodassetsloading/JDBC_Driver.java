package com.dsiq.prodassetsloading;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.google.common.base.Strings;

public class JDBC_Driver {
	Connection con;
	ResultSet rs;
	final static String zkQuorum_dev = "blvdevhdp04.ds-iq.corp";
	final static String zkQuorum_beta = "blvbetahdp08.ds-iq.corp";
	static String zkQuorum = zkQuorum_dev;
	public static String startASIN = " ";
	static final int DEFAULT_STAGING_BATCH_SIZE = 10000;
	
	static Connection connection = makeConnection();
	static Configuration configuration = Configuration.getInstance();
	
	final static String STAGING_AMZN_COLUMN_FAMILY = 
	"A.UPC AS A_UPC"
	+ ", A.SKU AS A_SKU"
	+ ", A.T AS A_T"
	+ ", A.CA AS A_CA"
	+ ", A.SC AS A_SC"
	+ ", A.DP AS A_DP"
	+ ", A.GL AS A_GL"
	+ ", A.AR AS A_AR"
	+ ", A.NR AS A_NR"
	+ ", A.H AS A_H"
	+ ", A.L AS A_L"
	+ ", A.W AS A_W"
	+ ", A.WT AS A_WT"
	+ ", A.AVT AS A_AVT"
	+ ", A.B AS A_B"
	+ ", A.PH AS A_PH"
	+ ", A.PL AS A_PL"
	+ ", A.PW AS A_PW"
	+ ", A.WG AS A_WG"
	+ ", A.PQ AS A_PQ"
	+ ", A.SZ AS A_SZ"
	+ ", A.CL AS A_CL"
	+ ", A.CP AS A_CP"
	+ ", A.LCP AS A_LCP"
	+ ", A.LP AS A_LP"
	+ ", A.LRP AS A_LRP"
	+ ", A.LUP AS A_LUP"
	+ ", A.SR AS A_SR"
	+ ", A.PN AS A_PN"
	+ ", A.PU AS A_PU"
	+ ", A.ST AS A_ST"
	+ ", A.MF AS A_MF"
	+ ", A.MDL AS A_MDL"
	+ ", A.MPN AS A_MPN"
	+ ", A.PD AS A_PD"
	+ ", A.TS AS A_TS"
	+ ", A.P AS A_P";
	
	final static String ASSET_SHARED_COLUMN_FAMILY = 
			  "S.M AS S_M"
			+ ", S.D AS S_D"
			+ ", S.B AS S_B"
			+ ", S.W AS S_W"
			+ ", S.L AS S_L"
			+ ", S.WI AS S_WI"
			+ ", S.H AS S_H"
			+ ", S.C AS S_C"
			+ ", S.Q AS S_Q"
			+ ", S.S AS S_S"
			+ ", S.U AS S_U";
	
	final static String ASSET_AMZN_COLUMN_FAMILY =
			  "A.A AS A_A"
			+ ", A.CA AS A_CA"
			+ ", A.SC AS A_SC"
			+ ", A.DP AS A_DP"
			+ ", A.GL AS A_GL"
			+ ", A.AR AS A_AR"
			+ ", A.NR AS A_NR"
			+ ", A.CP AS A_CP"
			+ ", A.LCP AS A_LCP"
			+ ", A.LP AS A_LP"
			+ ", A.LRP AS A_LRP"
			+ ", A.LUP AS A_LUP"
			+ ", A.SR AS A_SR"
			+ ", A.PN AS A_PN"
			+ ", A.PU AS A_PU"
			+ ", A.ST AS A_ST"
			+ ", MPN AS A_MPN"
			+ ", TS AS A_TS"
			+ ", A.U AS A_U"
	        + ", A.UD AS A_D";

/*	public JDBC_Driver(boolean isBeta) {
		zkQuorum = isBeta ? zkQuorum_beta : zkQuorum_dev;
	}*/

	public JDBC_Driver() {
	}
	
	static Connection getConnection() {
		if (connection == null) {
			return makeConnection();
		} else {
			return connection;
		}
	}

	static Connection makeConnection() {
		Connection conn = null;
		try {
			try {
				Class.forName("org.apache.phoenix.jdbc.PhoenixDriver");
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
			String connectionURL = "jdbc:phoenix:" + zkQuorum;
			conn = DriverManager.getConnection(connectionURL);
			conn.setAutoCommit(true);
		} catch (SQLException ex) {
			System.err.println("database connection: " + ex.getMessage());
		}
		return conn;
	}

	public static ResultSet getAssetRecord(String assetId) {
		String sql = "SELECT P, "  // row_key
				+ ASSET_SHARED_COLUMN_FAMILY
				+ ", "
				+ ASSET_AMZN_COLUMN_FAMILY
				+ ", S.AP AS S_AP"
				+ ", S.MSRP AS S_MSRP"
				+ ", S.PP AS S_PP"
				+ ", S.GTIN AS S_GTIN"
				+ ", W.SD AS W_SD"
				+ " FROM ASSET.PRODUCT WHERE P = ?"; 
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			stmt = connection.prepareStatement(sql);
			stmt.setString(1, assetId);
			rs = stmt.executeQuery();
		} catch (SQLException ex) {
			System.out.println("Exception running SQL: " + sql);
			ex.printStackTrace();
		}
		return rs;
	}
	
	public static ResultSet getStagingRecords() {
		int stagingBatchSize = DEFAULT_STAGING_BATCH_SIZE;
		String stagingBatchSizeStr = configuration.get(Configuration.RECORDS);
		if (!Strings.isNullOrEmpty(stagingBatchSizeStr)) {
			stagingBatchSize = Integer.valueOf(stagingBatchSizeStr);
		}
		String sql = "SELECT A, " // row_key
                + STAGING_AMZN_COLUMN_FAMILY
				+ " FROM STAGING_PRODUCT.AMZN"
				+ " WHERE A > ? ORDER BY A LIMIT ?";

		ResultSet rs = null;
		try {
			PreparedStatement stmt = connection.prepareStatement(sql);
			stmt.setString(1, startASIN);
			stmt.setInt(2,  stagingBatchSize);
		      rs = stmt.executeQuery(sql);			
		} catch (SQLException ex) {
			System.out.println("Exception executing SQL query: " + sql);
			ex.printStackTrace();
		}
		return rs;
	}
}
