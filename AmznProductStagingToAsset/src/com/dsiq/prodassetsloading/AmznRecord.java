package com.dsiq.prodassetsloading;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

public class AmznRecord {

	enum recordType {NEW, SAME_ASIN, MATCHED, UNKNOWN}

	recordType type;
	private BufferedWriter bw;
	private String filename;
	private final String dir = ".";
	
	private Path cwd;

	class AssetRecordStructure {
		String P;
		String S_M;
		String S_D;
		String S_B;
		String S_W;
		String S_L;
		String S_WI;
		String S_H;
		String S_C;
		String S_Q;
		String S_S;
		String S_U;

		String A_A;
		String A_CA;
		String A_SC;
		String A_DP;
		String A_GL;
		String A_AR;
		Integer A_NR;
		String A_CP;
		String A_LCP;
		String A_LP;
		String A_LRP;
		String A_LUP;
		String A_SR;
		String A_PN;
		String A_PU;
		String A_ST;
		String A_MPN;
		String A_TS;
		String A_U;
		String A_UD;

		String joinAsTSV() {
			StringBuffer sb = new StringBuffer();
			sb.append(P);
			sb.append('\t');
			sb.append(S_M);
			sb.append('\t');
			sb.append(S_D);
			sb.append('\t');
			sb.append(S_B);
			sb.append('\t');
			sb.append(S_W);
			sb.append('\t');
			sb.append(S_L);
			sb.append('\t');
			sb.append(S_WI);
			sb.append('\t');
			sb.append(S_H);
			sb.append('\t');
			sb.append(S_C);
			sb.append('\t');
			sb.append(S_Q);
			sb.append(S_S);
			sb.append('\t');
			sb.append(S_U);
			sb.append('\t');
			sb.append(A_A);
			sb.append('\t');
			sb.append(A_CA);
			sb.append('\t');
			sb.append(A_SC);
			sb.append('\t');
			sb.append(A_DP);
			sb.append('\t');
			sb.append(A_GL);
			sb.append('\t');
			sb.append(A_AR);
			sb.append('\t');
			sb.append(String.valueOf(A_NR));
			sb.append('\t');
			sb.append(A_CP);
			sb.append('\t');
			sb.append(A_LCP);
			sb.append('\t');
			sb.append(A_LP);
			sb.append('\t');
			sb.append(A_LRP);
			sb.append('\t');
			sb.append(A_LUP);
			sb.append('\t');
			sb.append(A_SR);
			sb.append('\t');
			sb.append(A_PN);
			sb.append('\t');
			sb.append(A_PU);
			sb.append('\t');
			sb.append(A_ST);
			sb.append('\t');
			sb.append(A_MPN);
			sb.append('\t');
			sb.append(A_TS);
			sb.append('\t');
			sb.append(A_U);
			sb.append('\t');
			sb.append(A_UD);
			return sb.toString();
		}
	}

	private AssetRecordStructure assetRecord = new AssetRecordStructure();

	public AmznRecord() {
		this.bw = null;
		this.filename = null;
		cwd = Paths.get(dir);
	}

	static public Integer getInteger( ResultSet rs, String strColName ) throws SQLException
	{
		int nValue = rs.getInt( strColName );
		return rs.wasNull() ? null : nValue;
	}

	public void setRecord(ResultSet rsStaging, recordType type, String row_key) {

		try {
			rsStaging.next();

			if (row_key == null)
				assetRecord.P = UUID.randomUUID().toString();
			else
				assetRecord.P = row_key;

			assetRecord.A_A = rsStaging.getString("A");
			assetRecord.A_CA = rsStaging.getString("A_CA");
			assetRecord.A_SC = rsStaging.getString("A_SC");
			assetRecord.A_DP = rsStaging.getString("A_DP");
			assetRecord.A_GL = rsStaging.getString("A_GL");
			assetRecord.A_AR = rsStaging.getString("A_AR");
			assetRecord.A_NR = getInteger(rsStaging, "A_NR");
			assetRecord.A_CP = rsStaging.getString("A_CP");
			assetRecord.A_LCP = rsStaging.getString("A_LCP");
			assetRecord.A_LP = rsStaging.getString("A_LP");
			assetRecord.A_LRP = rsStaging.getString("A_LRP");
			assetRecord.A_LUP = rsStaging.getString("A_LUP");
			assetRecord.A_SR = rsStaging.getString("A_SR");
			assetRecord.A_PN = rsStaging.getString("A_PN");
			assetRecord.A_PU = rsStaging.getString("A_PU");
			assetRecord.A_ST = rsStaging.getString("A_ST");
			assetRecord.A_MPN = rsStaging.getString("A_MPN");
			assetRecord.A_TS = rsStaging.getString("A_TS");
			assetRecord.A_U = rsStaging.getString("A_U");
			assetRecord.A_UD = rsStaging.getString("A_UD");

			assetRecord.S_M = rsStaging.getString("A_MF");
			assetRecord.S_D = rsStaging.getString("A_T");
			assetRecord.S_B = rsStaging.getString("A_B"); 
			assetRecord.S_W = rsStaging.getString("A_WG");
			assetRecord.S_L = rsStaging.getString("A_PL");
			assetRecord.S_WI = rsStaging.getString("A_PW");
			assetRecord.S_H = rsStaging.getString("A_PH");
			assetRecord.S_C = rsStaging.getString("A_CL");
			assetRecord.S_Q = rsStaging.getString("A_PQ");
			assetRecord.S_S = rsStaging.getString("A_SZ");
			assetRecord.S_U = null; // not used
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}


	public void concludeFile() {
		if (bw != null) {
			try {
				bw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (filename != null) {
			try {
				@SuppressWarnings("unused")
				Process p = new ProcessBuilder().inheritIO().command("./UpsertToProductAsset.sh " + filename).start();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void startFile() {
		concludeFile();
		filename = new SimpleDateFormat("yyyyMMddhhmm'.txt'").format(new Date());
		FileWriter fw = null;
		try {
			Path p = cwd.resolve(filename).toAbsolutePath();
			fw = new FileWriter(p.toString());
			bw = new BufferedWriter(fw);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void append() {
		String tsvLine = assetRecord.joinAsTSV();
		try {
			bw.append(tsvLine);
			bw.newLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
