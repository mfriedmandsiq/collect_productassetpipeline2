package com.dsiq.prodassetsloading;

import org.junit.Test;

public class UPCFixer {
	public static String fixUPC(int upc) {
		return fixUPC(Integer.toString(upc));
	}
	
	public static String fixUPC(String upc) {
		if (upc.length() == 12) {
			return upc;
		} else if (upc.length() == 10) {
			upc = "0" + upc;
		}
		if (upc.length() != 11) {
			return null; // invalid UPC
		}
		int checkDigit = 0;
		int sumOddNumbs = 0, sumEvenNumbs = 0;
		char[] chUPC = upc.toCharArray();
		int intChar = 0;
		for (int i = 0; i < 11; i++) {
			intChar = Integer.parseInt(Character.toString(chUPC[i]));
			if ((i + 1)%2 == 0) {
				sumEvenNumbs += intChar;
			} else {
				sumOddNumbs += intChar;
			}
		}
		checkDigit = 10 - ((sumOddNumbs*3 + sumEvenNumbs)%10);
		upc =  upc + checkDigit;
		return upc;
	}
	
	@Test
	public void testUPCFixer() {
		String fixedUPC = null;
		String[] upcs = { "3500068067", "3600025830", "3600025832", "3600037387", "3600037392", "3700035520", "30045019204"};
		for (int i = 0; i < upcs.length; i++) {
			fixedUPC = fixUPC(upcs[i]);
			System.out.println("UPC = " + fixedUPC + ", (" + upcs[i] + ")");
		}
		/* 
		Results:
		UPC = 035000680679, (3500068067)
		UPC = 036000258301, (3600025830)
		UPC = 036000258325, (3600025832)
		UPC = 036000373875, (3600037387)
		UPC = 036000373929, (3600037392)
		UPC = 037000355205, (3700035520)
		UPC = 300450192042, (30045019204)
		*/	
	}
}
