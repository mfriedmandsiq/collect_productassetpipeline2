package com.dsiq.prodassetsloading;

import java.util.ArrayList;

import org.junit.Test;

public class LetterPairSimilarity {
	private static String[] letterPairs(String str) {
		int numPairs = str.length()-1;
		String[] pairs = new String[numPairs];
		for (int i=0; i<numPairs; i++) {
			pairs[i] = str.substring(i,i+2);
		}
		return pairs;
	}
	
	private static ArrayList<String> wordLetterPairs(String str) {
		ArrayList<String> allPairs = new ArrayList<String>();
		// Tokenize the string and put the tokens/words into an array
		String[] words = str.split("\\s");
		// For each word
		for (int w=0; w < words.length; w++) {
			// Find the pairs of characters
			String[] pairsInWord = letterPairs(words[w]);
			for (int p=0; p < pairsInWord.length; p++) {
				allPairs.add(pairsInWord[p]);
			}
		}
		return allPairs;
	}
	
	public static double compareStrings(String str1, String str2) {
		ArrayList<String> pairs1 = wordLetterPairs(str1.toUpperCase());
		ArrayList<String> pairs2 = wordLetterPairs(str2.toUpperCase());
		int intersection = 0;
		int union = pairs1.size() + pairs2.size();
		for (int i=0; i<pairs1.size(); i++) {
			Object pair1=pairs1.get(i);
			for(int j=0; j<pairs2.size(); j++) {
				Object pair2=pairs2.get(j);
				if (pair1.equals(pair2)) {
					intersection++;
					pairs2.remove(j);
					break;
				}
			}
		}
		return (2.0*intersection)/union;
	}
	
	@Test
	public void test() {
		String s0 = "Web Database Applications";
		String[] ss = {"Web Database Applications", "Web Database Applications with PHP & MySQL", "Creating Database Web Applications with PHP and ASP", "Building Database Applications on the Web Using PHP3",
				"Building Web Database Applications with Visual Studio 6","Web Application Development With PHP", "WebRAD: Building Database Applications on the Web with Visual FoxPro and Web Connection",
				"Structural Assessment: The Role of Large and Full-Scale Testing","How to Find a Scholarship Online","Les Mis��rables"};
		double r = 0.0;
		for (int i = 0; i < ss.length; i++) {
			r = compareStrings(s0, ss[i]);
			System.out.println(ss[i] + "; r = " + r);
		}
		r = compareStrings("KNOLL VC105PM TRANSFORMER FREE ROTARY VOLUME CONTROL", "KNOLL SYSTEM VC105PM TRANSFORMER FREE ROTARY VOLUME CONTROL");
		System.out.println("; r = " + r);
	}
	
	@Test
	public void test2() 
	{
		String s0 = "BERNAT HANDICRAFTER COTTON YARN";
		String[] ss1 = {
			"BERNAT HANDICRAFTER SCENT COTTON YARN",
			"BERNAT HANDICRAFTER COTTON YARN COTTAGE TWIST",
			"BERNAT HANDICRAFTER COTTON YARN OMBRE PRINT MOON DANCE OMBRE",
			"BERNAT HANDICRAFTER HOLIDAY SPARKLE CHRISTMAS YARN",
			"HANDICRAFTER COTTON YARN 340 GRAM",
			"BERNAT HANDICRAFTER COTTON YARN SOLID TWIST",
			"BERNAT HANDICRAFTER COTTON YARN",
			"BULK BUY BERNAT HANDICRAFTER COTTON YARN 400 GRAM WHITE 162027-2 2-PACK",
			"HANDICRAFTER COTTON YARN OMBRE PRINTS FRUIT PUNCH",
			"BERNAT HANDICRAFTER HOLIDAY TWIST CHRISTMAS YARN YULETIDE TWIST",
			"HANDICRAFTER COTTON YARN SOLIDS FRENCH BLUE",
			"BULK BUY BERNAT HANDICRAFTER COTTON YARN 400 GRAMS COUNTRY RED 2-PACK"};
		String[] ss = ss1;
		double r = 0.0;
		System.out.println("======== " + s0 + " ========");
		for (int i = 0; i < ss.length; i++) {
			r = compareStrings(s0, ss[i]);
			System.out.println(ss[i] + "; r = " + r);
		}
		
		s0 = "FATHEAD JUNIOR ADRIAN PETERSON";
		String[] ss2 = {"MINNESOTA VIKING ADRIAN PETERSON JUNIOR WALL DECAL","ADRIAN PETERSON FATHEAD JUNIOR WALL DECAL 26 32", "FATHEAD JUNIOR MINNESOTA VIKING ADRIAN PETERSON WALL GRAPHIC"};
		ss = ss2;
		System.out.println("======== " + s0 + " ========");
		for (int i = 0; i < ss.length; i++) {
			r = compareStrings(s0, ss[i]);
			System.out.println(ss[i] + "; r = " + r);
		}
		s0 = "PRIMO BOTTOM LOADING HOT WATER DISPENSER";
		String[] ss3 = {"PRIMO 601089 HOT ROOM TEMPERATURE COLD BOTTOM LOADING WATER DISPENSER",
				"PRIMO BOTTOM LOAD WATER COOLER STAINLESS STEEL 3 5 GALLON",
				"VI TAPER VWD5446BLS FULL SIZE WATER DISPENSER",
				"HOT ROOM WATER COOLER",
				"SOLEUS AIR WA2-02-50 AQUA WATER COOLER HOT COLD SETTING BOTTOM LOAD DESIGN",
				"AVAUNT ENERGY SAVER WATER DISPENSER",
				"PRIMO BLACK BOTTOM LOAD BOTTLE WATER DISPENSER 3 5 GALLON",
				"PRIMO 601000 INDUSTRIAL HOT COLD BOTTOM LOADING WATER DISPENSER",
				"PRIMO 601088 HOT COLD BOTTOM LOADING WATER DISPENSER",
				"PRIMO 601001 INDUSTRIAL HOT COLD BOTTOM LOADING WATER DISPENSER COFFEE MAKER",
				"PRIMO 601001 FLEX HD BOTTOM LOADING WATER COOLER DRIP COFFEE BREWER"};
		ss = ss3;
		System.out.println("======== " + s0 + " ========");
		for (int i = 0; i < ss.length; i++) {
			r = compareStrings(s0, ss[i]);
			System.out.println(ss[i] + "; r = " + r);
		}
		s0 = "SALLY HANSEN DIAMOND STRENGTH CHIP NAIL COLOR LAVENDER MARQUIS";
		String[] ss4 = {"SALLY HANSEN DIAMOND STRENGTH NAIL COLOR 0.45 FL OZ SET 4, 22 COLOR CHOOSE",
				"SALLY HANSEN DIAMOND STRENGTH CHIP NAIL COLOR LAVENDER MARQUIS 0.45 OZ PACK 2",
				"SALLY HANSEN DIAMOND STRENGTH CHIP NAIL COLOR LAVENDER MARQUIS",
				"SALLY HANSEN DIAMOND STRENGTH NAIL COLOR 0.45 FL OZ SET 6, 22 COLOR CHOOSE",
				"SALLY HANSEN DIAMOND STRENGTH CHIP NAIL COLOR LAVENDER MARQUIS 0.45 OZ",
				"SALLY HANSEN DIAMOND STRENGTH NAIL COLOR 0.45 FL OZ SET 2, 22 COLOR CHOOSE",
				"SALLY HANSEN DIAMOND STRENGTH NAIL COLOR 0.45 FL OZ SET 1, 22 COLOR CHOOSE",
				"SALLY HANSEN DIAMOND STRENGTH NAIL COLOR 0.45 FL OZ SET 6, 22 COLOR CHOOSE",
				"SALLY HANSEN DIAMOND STRENGTH NAIL COLOR 0.45 FL OZ SET 1, 22 COLOR CHOOSE"};
		ss = ss4;
		System.out.println("======== " + s0 + " ========");
		for (int i = 0; i < ss.length; i++) {
			r = compareStrings(s0, ss[i]);
			System.out.println(ss[i] + "; r = " + r);
		}
	}
}
