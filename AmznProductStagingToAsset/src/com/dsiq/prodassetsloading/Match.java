package com.dsiq.prodassetsloading;

import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;

import com.dsiq.prodassetsloading.MatcherUtils.typelist;
import com.google.common.base.Strings;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author mfriedman.adm
 *
 */
public class Match {
	WordFilter oWordFilter;
	MatcherUtils matcherUtils;
	Utils utils;
	Configuration configuration;

	public Match(){
		configuration = Configuration.getInstance();
		matcherUtils =  new MatcherUtils();
		oWordFilter = new WordFilter();
		utils = new Utils();
	}

	public void matchStagingRecordsToAsset(ResultSet rsStagingRecords)
	{
		try {
			while (rsStagingRecords.next()) {
				processOneStagingRecord(rsStagingRecords);
			}
		} catch (SQLException e) {
			System.out.println("Location C exception");
			e.printStackTrace();
		}
	}

	public void processOneStagingRecord(ResultSet rsStagingRecord) {
		String stagingProductDescription;
		String stagingAsin;
		String stagingTitle;
		String finalDescription;
		SolrDocumentList assetPotentialMatchList = null;
		try {
			stagingTitle = utils.emptyIfNull(rsStagingRecord.getString("A_T"));
			stagingProductDescription = utils.emptyIfNull(rsStagingRecord.getString("A_PD"));
			if (Strings.isNullOrEmpty(stagingTitle)) {
				if (Strings.isNullOrEmpty(stagingProductDescription)) {
					finalDescription = null;
				} else {
					finalDescription = stagingProductDescription;
				}
			} else {
				if (Strings.isNullOrEmpty(stagingProductDescription)) {
					finalDescription = stagingTitle;
				} else {
					finalDescription = stagingProductDescription;
				}
			}
			assetPotentialMatchList = matcherUtils.getSolrMatches(finalDescription);
			stagingAsin = utils.emptyIfNull(rsStagingRecord.getString("A"));
			JDBC_Driver.startASIN = stagingAsin;
			String[] stagingFieldValues = MatcherUtils.parseValues(rsStagingRecord, typelist.STAGING);
			findMatch(stagingAsin, assetPotentialMatchList, stagingFieldValues);
		} catch (Exception e) {
			System.out.println("Location E exception");
			e.printStackTrace();
		}
	}

	public void findMatch(String stagingAsin, SolrDocumentList assetProdList, String[] stagingFieldValues)
	{
		for (int assetRec = 0; assetRec < Math.min(assetProdList.size(), 10); assetRec++) {
			SolrDocument assetSolrDoc = assetProdList.get(assetRec);

			String assetUUID = (String) assetSolrDoc.getFieldValue("id");

			String assetAmznAsin = utils.emptyIfNull(assetSolrDoc.getFieldValue("asin")); // A.A
			String assetDescription = utils.emptyIfNull(assetSolrDoc.getFieldValue("desc")); // S.D
			String assetBrand = utils.emptyIfNull(assetSolrDoc.getFieldValue("brand")); // S.B
			String assetWmtCat = utils.emptyIfNull(assetSolrDoc.getFieldValue("cat")); // W.CD

		    String[] assetFieldValues = null;
			String assetWmtSignDesc = null;
			double assetPrice = 0.0;
			String assetAmznUPC = null;
			String assetSharedUPC = null;
			ResultSet assetRS = JDBC_Driver.getAssetRecord(assetUUID);

			try {
				if (assetRS.next()) {
					assetFieldValues = MatcherUtils.parseValues(assetRS, typelist.ASSET);
					assetSharedUPC = assetFieldValues[MatcherUtils.ASSET_S_GTIN];

					assetWmtSignDesc = utils.emptyIfNull(assetFieldValues[MatcherUtils.ASSET_W_SD]);
					assetPrice = Double.valueOf(assetFieldValues[MatcherUtils.ASSET_S_AP]);
					assetAmznUPC = assetFieldValues[MatcherUtils.ASSET_A_U];
				}
			} catch (SQLException e) {
				System.out.println("Location L exception");
				e.printStackTrace();
			}
			String stagingAmznDescription = null;
			double stagingAmznPrice = 0.0;
			String stagingAmznBrand = null;

			MatchingInfo oMatchingInfo = new MatchingInfo(assetAmznAsin,
					assetSharedUPC, assetWmtSignDesc, assetBrand, assetAmznUPC,
					stagingAmznDescription, stagingAmznBrand, assetPrice,
					stagingAmznPrice, this);

			if (oMatchingInfo.isMatchingProduct) {
				// System.out.println("FOUND MATCH \n");
				try {
					// Update Shared and AMZN column families
					matcherUtils.amznMatchingToExistingAssetEntry(stagingFieldValues, assetFieldValues, assetUUID);
				} catch (Exception e) {
					System.out.println("Location P exception");
					e.printStackTrace();
				}
				System.out.println("Matched : " + stagingAsin + " "
						+ stagingAmznDescription + " with \n \t" + assetUUID
						+ " " + assetWmtSignDesc + "\n");
				return;
			}

			}

	matcherUtils.amznNewAssetEntry(stagingFieldValues);
	}

	public void processStaging(){
		ResultSet res;

		this.matcherUtils = new MatcherUtils();
		try {
			res = JDBC_Driver.getStagingRecords();
			matchStagingRecordsToAsset(res);
		} catch (Exception e) {
			System.out.println("Location R exception");
			e.printStackTrace();
		}
		System.out.println("All staging records processed.");
		matcherUtils.close();
	}


	public void close() throws Exception {
		matcherUtils.close();
	}

	public double compareStrings(String s10, String s20, boolean removeStopWords)
	{
		if (!removeStopWords) {
			oWordFilter.removeUnwantedWords = false;
		} else {
			oWordFilter.removeUnwantedWords = true;
		}
		String s1 = s10;
		String s2 = s20;
		try {
			s1 = oWordFilter.removeUnwantedWordsAndStandardizeUnits(s10);
			s2 = oWordFilter.removeUnwantedWordsAndStandardizeUnits(s20);
			// System.out.println("STRING PROCESSING SUCCESSFUL");
		} catch (java.lang.ArrayIndexOutOfBoundsException e) {
			System.out.println("BAD STRING FOUND: " + s20 + "\n" + "or" + s10
					+ "\n");
		} catch (java.lang.NumberFormatException e) {
			System.out
			.println("\"java.lang.NumberFormatException: multiple points\" on inputs "
					+ s10 + " and " + s20 + "\n");
		}
		try {
			return LetterPairSimilarity.compareStrings(s1, s2);
		} catch (java.lang.NegativeArraySizeException e) {
			System.out.println("LETTER PAIR ERROR ON: " + s20 + " and " + s10
					+ "\n");
			return 0.0;
		}
	}

	public double getPriceMatchingScore(double p1, double p2) {
		double mean = 0.5 * (p1 + p2);
		double arg = (p1 - p2) / mean;
		arg *= arg;
		double priceMatchingScore = Math.exp(-arg);
		return priceMatchingScore;
	}

}
