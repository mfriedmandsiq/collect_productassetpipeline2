package com.dsiq.prodassetsloading;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;


public class Configuration extends HashMap<String, String> {
    /**
	 * 
	 */
	
	private static Configuration config = null;
	
	public static Configuration getInstance() {
		if (config == null) {
			config = new Configuration();
		}
		return config;
	}
	
	public static final String RECORDS = "records";
	public static final String TITLE_MATCH_THRESHOLD = "titleMatchThreshold";
	public static final String PRICE_MATCH_THRESHOLD = "priceMatchThreshold";
	public static final String UPC_TITLE_MATCH_THRESHOLD = "upcTitleMatchThreshold";
	public static final String UPC_PRICE_MATCH_THRESHOLD = "upcPriceMatchThreshold";	
	
	public static final String RECORDS_DEFAULT = "10000";
	public static final String TITLE_MATCH_THRESHOLD_DEFAULT = "0.95";
	public static final String PRICE_MATCH_THRESHOLD_DEFAULT = "0.9";
	public static final String UPC_TITLE_MATCH_THRESHOLD_DEFAULT = "0.8";
	public static final String UPC_PRICE_MATCH_THRESHOLD_DEFAULT = "0.5";	

	private static final long serialVersionUID = 1L;
	private final String configDir = "/etc/DS-IQ/AssetApi/";
	private final String configFile = "app.config/";
    
    public Configuration() {
    	super();
    	Properties properties = new Properties();

    	String configPath = configDir + configFile;
		File file = new File(configPath);

        boolean useDefaults = false;
		FileReader fr = null;
		try {
			fr = new FileReader(file);
		} catch (FileNotFoundException e) {
			System.out.println("WARNING: No config file not found at " + file.getAbsolutePath() + ". Using default values.");
			useDefaults = true;
		}
		try {
			properties.load(fr);
		} catch (IOException e) {
			e.printStackTrace();
			useDefaults = true;
		}

		String records;
		String titleMatchThreshold;
		String priceMatchThreshold;
		String upcTitleMatchThreshold;
		String upcPriceMatchThreshold;

        if (useDefaults) {
        	records = RECORDS_DEFAULT;
        	titleMatchThreshold = TITLE_MATCH_THRESHOLD_DEFAULT;
        	priceMatchThreshold = PRICE_MATCH_THRESHOLD_DEFAULT;
        	upcTitleMatchThreshold = UPC_TITLE_MATCH_THRESHOLD_DEFAULT;
        	upcPriceMatchThreshold = UPC_PRICE_MATCH_THRESHOLD_DEFAULT;
        }
        else {
    		records = (String) properties.get(RECORDS);
    		titleMatchThreshold = (String) properties.get(TITLE_MATCH_THRESHOLD);
    		priceMatchThreshold = (String) properties.get(PRICE_MATCH_THRESHOLD);
    		upcTitleMatchThreshold = (String) properties.get(UPC_TITLE_MATCH_THRESHOLD);
    		upcPriceMatchThreshold = (String) properties.get(UPC_PRICE_MATCH_THRESHOLD);
        }
		
		put(RECORDS, records);
		put(TITLE_MATCH_THRESHOLD, titleMatchThreshold);
		put(PRICE_MATCH_THRESHOLD, priceMatchThreshold);
		put(UPC_TITLE_MATCH_THRESHOLD, upcTitleMatchThreshold);
		put(UPC_PRICE_MATCH_THRESHOLD, upcPriceMatchThreshold);
		
		try {
			fr.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
    }

}
