package com.dsiq.prodassetsloading;
import org.apache.commons.lang.StringUtils;

import com.dsiq.prodassetsloading.Match;

public class MatchingInfo implements Comparable<MatchingInfo> {
	//"Asin\tWmtUpc\tWmtSignDesc\tWmtBrand\tWmtPrice\tAmznUpc\tAmznTitle\tAmznBrand\tAmznPrice\tTitle Match Score\tPrice Match Score\tIsMatchProduct"
	public String asin, wmtUpc, wmtSignDesc, wmtBrand;
	public String amznUpc, amznTitle, amznBrand;
	public double wmtPrice, amznPrice, titleMatchScore, priceMatchScore;
	public boolean isMatchingProduct, upcMatch;

	static final double NOT_DEFINED = 0;
	Configuration config;

	//changed last argument to AssetProductMatch from MatchingInfo so as to be compatible with new code
	public MatchingInfo(String asin, String wmtUpc, String wmtSignDesc, String wmtBrand, String amznUpc, String amznTitle, String amznBrand,
			double wmtPrice, double amznPrice, Match oWmtAmznMatch)
	{
		this.asin = asin;
		this.wmtUpc = wmtUpc;
		this.wmtSignDesc = wmtSignDesc;
		this.wmtBrand = wmtBrand;
		this.amznUpc = amznUpc;
		this.amznTitle = amznTitle;
		this.amznBrand = amznBrand;
		this.wmtPrice = wmtPrice;
		this.amznPrice =amznPrice;
		this.config = Configuration.getInstance();
		upcMatch = false;
		if (amznUpc != null){
			//System.out.println("Has Amazon UPC! " + amznUpc + "\n");
			amznUpc = StringUtils.leftPad(amznUpc,14,'0');
		}

		if (wmtUpc!=null && wmtUpc.length() > 0 && wmtUpc.equals(amznUpc)) {
			//System.out.println("Matched on UPC!\n");
			upcMatch = true;
		}
		if (wmtSignDesc!=null && wmtSignDesc.length() > 0 && amznTitle.length() > 0) {
			titleMatchScore = oWmtAmznMatch.compareStrings(wmtSignDesc, amznTitle, true);
		} else {
			titleMatchScore = NOT_DEFINED;
		}
		if (wmtPrice > 0 && amznPrice > 0) {
			priceMatchScore = oWmtAmznMatch.getPriceMatchingScore(wmtPrice, amznPrice);
		} else {
			priceMatchScore = NOT_DEFINED;
		}
		if (amznBrand!=null && amznBrand.equalsIgnoreCase(wmtBrand)) {
			titleMatchScore = Math.sqrt(titleMatchScore);
		}
		isMatchingProduct = false;
		if (upcMatch) {
			if (titleMatchScore > Double.parseDouble(config.get(Configuration.UPC_TITLE_MATCH_THRESHOLD)/* && Double.parseDouble(config.get(Configuration.UPC_PRICE_MATCH_THRESHOLD) > 0.5*/)) {
				isMatchingProduct = true;
			}
		} else {
			if (titleMatchScore > Double.parseDouble(config.get(Configuration.TITLE_MATCH_THRESHOLD)) && priceMatchScore > Double.parseDouble(config.get(Configuration.PRICE_MATCH_THRESHOLD))) {
				isMatchingProduct = true;
			}
		}
	}


	@Override
	public int compareTo(MatchingInfo other)
	{
		double thisScore = this.upcMatch? 1 : 0 + this.titleMatchScore + this.priceMatchScore;
		double otherScore = other.upcMatch? 1 : 0 + other.titleMatchScore + other.priceMatchScore;
		if (thisScore > otherScore) {
			return 1;
		} else if (thisScore == otherScore) {
			return 0;
		} else {
			return -1;
		}
	}
}