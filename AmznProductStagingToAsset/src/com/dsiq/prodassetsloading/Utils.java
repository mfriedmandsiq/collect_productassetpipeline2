package com.dsiq.prodassetsloading;

public class Utils {
	
	public String emptyIfNull(Object obj) {
		if (obj == null)
			return "";
		else if (obj instanceof String)
			return (String) obj;
		return "";
	}

	public Float zeroIfNull(Float float1) {
		if (float1 == null)
			return 0f;
		else
			return float1;
	}
}
