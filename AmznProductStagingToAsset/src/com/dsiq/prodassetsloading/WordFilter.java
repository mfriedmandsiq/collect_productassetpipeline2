package com.dsiq.prodassetsloading;

import java.util.Arrays;
import java.util.HashSet;

//import keywordsmatch.WordFilter;

public class WordFilter {
	private HashSet<String> stopWords; //Stop words
	String[] sortedUnwantedWords;
	StandardizeUnits standUnits;
	public boolean removeUnwantedWords;
	
	public WordFilter(String[] unWatedWords) throws Exception
	{
		initInstance(unWatedWords);
	}
	
	public WordFilter()
	{
		initInstance(null);
	}
	
	private void initInstance(String[] unWatedWords)
	{
		removeUnwantedWords = true;
		standUnits = new StandardizeUnits();
		stopWords = StopWords.getStopWords();
		char c;
		String s;
		for (int i = 33; i <= 47; i++) {
			c = (char)i;
			s = "" + c;
			stopWords.add(s);
		}
		if (unWatedWords != null) {
			for (String unwantedWord : unWatedWords) {
				if (!isUnwanteWord(unwantedWord.toUpperCase())) {
					stopWords.add(unwantedWord.toUpperCase());
				}
			}
		}
		Object[] oUnwantedWords = stopWords.toArray();
		sortedUnwantedWords = new String[oUnwantedWords.length];
		for (int i = 0; i < oUnwantedWords.length; i++) {
			sortedUnwantedWords[i] = ((String)oUnwantedWords[i]).toUpperCase();
		}
		Arrays.sort(sortedUnwantedWords);
	}
	
	public String fixWord(String word_in) {
		String word = word_in.toLowerCase();
		String newWord = null;
		newWord = Autocorrection.autoCorrect(word).toUpperCase();
		return newWord;
	}
	
	public String removeUnwantedWords(String line) throws Exception
	{
		return removeUnwantedWordsAndStandardizeUnits(line, false);
	}
	
	public String removeUnwantedWordsAndStandardizeUnits(String line)
	{
		return removeUnwantedWordsAndStandardizeUnits(line, true);
	}
	
	public String removeUnwantedWordsAndStandardizeUnits(String line, boolean standardizeUnits)
	{
		//System.out.println("Line is: " + line + "\n");
		if (line == null || line.trim().length() == 0) {
			return "";
		}
		String[] tokens = line.split(" ");
		int len = tokens.length;
		int[] keep = new int[len];
		for (int i = 0; i < len; i++) {
			if (tokens[i].trim().length() == 0) {
				keep[i] = 0;
				continue;
			}
			tokens[i] = myTrim(tokens[i]);
			tokens[i] = fixWord(tokens[i]);
			if (tokens[i].length() == 0) {
				keep[i] = 0;
			} else if (this.removeUnwantedWords && isUnwanteWord(tokens[i])){
					keep[i] = 0;
					if ((tokens[i].equals("\"") || tokens[i].equals("'")) && i > 0) {
						tokens[i - 1] += tokens[i];
						tokens[i] = "";
					}
			} else {
				keep[i] = 1;
			}
		}
		int indx = 0;
		while (indx < len) {
			if (keep[indx] == 1) {
				break;
			}
			indx++;
		}
		if (len <= 3) {
			indx = 0;
			for (int i = 0; i < len; i++) {
				keep[i] = 1;
			}
		}
		String newLine = tokens[indx];
		for (int i = indx + 1; i < len; i++) {
			if (keep[i] == 1 && tokens[i].length() > 0) {
				newLine += " " + tokens[i];
			}
		}
		if (standardizeUnits) {
			if (this.standUnits.hasUnitsInTitle(newLine)) {
				newLine = this.standUnits.fixUnitsInTitle(newLine);
			}
		}
		return newLine;
	}
	
	public static String myTrim2(String s) {
		int beginIndex = 0, endIndex = s.length() - 1;
		char c_left = s.charAt(beginIndex);
		char c_right = s.charAt(endIndex);
		while (c_left == '(' || c_left == '[' || c_left == '<' || c_left == '{') {
			beginIndex++;
			c_left = s.charAt(beginIndex);
		}
		while (c_right == ')' || c_right == ']' || c_right == '>' || c_right == '}') {
			endIndex--;
			c_right = s.charAt(endIndex);
		}
		return s.substring(beginIndex, endIndex + 1);
	}
	
	public static String myTrim(String s) {
		s = s.toUpperCase().trim();
		if (s.equals("\"") || s.equals("\"\"")) { 
			return s;	// inch
		}
		if (s.equals("'") || s.equals("''")) { 
			return s;	// foot or inch
		}
		char[] cs = s.toCharArray();
		if (isNumberical(cs[0])) {
			return myTrim2(s); 
		}
		String s_new = "";
		int i_max = cs.length - 1;
		for (int i = 0; i <= i_max; i++) {
			if (isNumberical(cs[i])) {
				return myTrim2(s);
			}
			if (i > 0 && i < i_max && (cs[i] == '-' || cs[i] == '/')) {
				s_new += cs[i];
				continue;
			}
			if (i == i_max - 1 && cs[i] == '\'') {
				s_new += '\'';
				continue;
			} 
			if (isAlphabet(cs[i])) {
				s_new += cs[i];
			}
			
		}
		int iFirstChar = 0;
		char firstChar = s_new.charAt(iFirstChar);
		int iLastChar = s_new.length() - 1;
		while ((firstChar >= 33 && firstChar <= 47) || (firstChar >= 91 && firstChar <= 96) || (firstChar >= 123 && firstChar <= 126)) {
			iFirstChar++;
			if (iFirstChar > iLastChar) {
				iFirstChar--;
				break;
			}
			firstChar = s_new.charAt(iFirstChar);
		}
		char lastChar = s_new.charAt(iLastChar);
		while ((lastChar >= 33 && lastChar <= 47) || (lastChar >= 91 && lastChar <= 96) || (lastChar >= 123 && lastChar <= 126)) {
			iLastChar--;
			if (iFirstChar > iLastChar) {
				iLastChar++;
				break;
			}
			lastChar = s_new.charAt(iLastChar);
		}
		s_new = s_new.substring(iFirstChar, iLastChar + 1);
		if (s_new.endsWith("'S")) {
			s_new = s_new.substring(0, s_new.length() - 2);
		}
		return s_new;
	}
	
	public static boolean isAlphabet(char c) {
		c = Character.toLowerCase(c);
		if (c < 'a' && c > 'z') {
			return false;
		} else {
			return true;
		}
	}
	
	public static boolean isNumberical(char c) {
		if (c >= '0' && c <= '9') {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean isUnwanteWord (String word) 
    {
		if (sortedUnwantedWords == null) {
			return false;
		}
		word = word.toUpperCase();
        int lo = 0, mid = 0;
        int hi = sortedUnwantedWords.length - 1;
        String unwanteWord = null;
        while (lo <= hi) {
            mid = lo + (hi - lo) / 2;
            unwanteWord = this.sortedUnwantedWords[mid];
            if (word.compareTo(unwanteWord) < 0) {
            	hi = mid - 1;
            } else if (word.compareTo(unwanteWord) > 0) {
            	lo = mid + 1;
            } else {
            	return true;
            }
        }
        return false;
    }
	
	public static void testMe() throws Exception {
		String[] lines = {
				"Stitch 14.2\"X15.4 \"",
				"Cross Stitch 14.2\"X15.4 \"",//Beau Rivage Childhood Story
				"2'3\"x8'",
				"2'3\" x 8'",
				"Nourison Graphic Illusions Loop Pile Crisscross Runner Rug, Ivory, 2'3\" x 8'",
				"Summer Escapes A and C Cartridges, Twin-Pack",
				"OmniMount PLAY70 37\" to 60\" Interactive Flat Panel Mount, Silver",
				"Regenerators Doctor Octopus, 1/24 Spider-Man and 1/64 Spider-Man Vehicles, Set of 3",
				"Smucker&#39;s: Sweet Orange Marmalade, 18 Oz",
				"Calgon Musk Body Mist, 8 oz",
				"Fisher-Price iXL Toy Story Software",
				"Dimensions Feltworks Medium Felt Bowl, 7.5\" diameter, Green",
				"Kvuzat Yavne Pickled Cucumbers In Vinegar, 19 oz (Pack of 12)",
				"Silvalume Flexible Afghan Hook, 22\", Size J10, Light Yellow",
				"Logo Chair NCAA Central Florida Sherpa Blanket",
				"Castle Logix",
				"Hard Candy Plumping Serum-Lip Gloss, Purple",
				"Bernat Handicrafter Cotton Yarn",
				"Targus SafePORT Rugged Case for iPhone 5/5s",
				"18\" My Life As Cowgirl Dressed Doll",
				"Sassy - Sensory Symphony",
				"Wilton Thank You Card Kit, 50/pkg, Expression Of Love",
				"FurReal Friends Tea Cup Pups - Yorkie Puppy",
				"FurReal Friends Walking Snuggimal Asst",
				"Trademark Fine Art 'Le Frou Frou' Matted Framed Art by Lucien Henri Weiluc",
				"Trademark Global Hunting Camo 31\" Padded Swivel Bar Stool",
				"John Deere Tractor Replica with Blade and Plow",
				"Knoll VC105PM Transformer Free Rotary Volume Control",
				"Forces of Valor German Tiger I Normandy, 1944 (D-Day Series)",
				"Lenovo 15.6\" ThinkPad T530 23592HU Laptop PC with Intel Core i5-3320M Processor and Windows 7 Professional",
				"Premier Zurich Platform Bed Frame, Twin, Brown, Box 2 of 3",
				"International Silver 44-Piece Smithfield Flatware Set",
				"3M Scientific Anglers Supra Floating Lines With Loop, 9-F, Buckskin WF",
				"Captain America: The First Avenger - Concept Series Deluxe Mission Pack, Captain America Fortress Assault Mission",
				"OFM Super Task Computer Chair",
				"Mainstays Towel Bar",
				"JT&T Products Universal Single Contact 'Snap-In' Light Socket",
				"Rust-Oleum Patch and Crack Filler Concrete Repair Kit",
				"Riders - Women's Eased Fit Jeans",
				"Lodge Enamel Cast Iron 7.5-Quart Dutch Oven",
				"Canon Vixia HF R42 High Definition Video Lens Digital Camcorder with 32x Optical Zoom, 3\" LCD and Image Stabilization",
				"Simply Dog Grey Reflective Tech Dog Jacket, Navy, (Multiple Sizes Available)",
				"Iman Pressed Powder",
				"Moultrie Game Spy I-45S IR 4.0 Megapixel Game Camera",
				"Wilson Hex Evo Soccer Ball, Silver/Red, Size 5",
				"Garland Zebra Patterned Woven Olefin Area Rug",
				"Lactulose 10gm/15 syrup",
				"Snow's Clam Juice, 8 fl oz, (Pack of 12)",
				"Life Gear Glow LED Flashlight Key Chain",
				"Vogue Pattern Misses' Fitting Shell, (22)",
				"1/4 Carat T.W. Princess and Round-Cut Diamond Engagement Ring in 10k White Gold",
				"Fathead Junior Adrian Perterson",
				"Yum-a-Roo's Tropical Twist & Caribbean Crop Dried Fruit & Veggie Bites Variety Pack, 0.75 oz, (Pack of 6)",
				"Premier Arctic Dragon Mask Adult Halloween Accessory",
				"Philips Norelco Precision Ear & Nose Trimmer Set, NT9110/70",
				"Bodysuit Teen Halloween Costume",
				"Household Essentials Banana Leaf Toilet Paper Holder with Lid, Natural",
				"Mother's Quick Cooking Barley, 11 oz, (Pack of 12)",
				"Lion Brand Fun Fur Yarn, Available in Multiple Colors",
				"X Digital Media 4 GB 300X CompactFlash Maximus Card",
				"Stack-On Executive Safe with Electronic Lock",
				"Rusk Keratin Deepshine Smooth Shampoo, 12 oz",
				"Trademark Fine Art \"Colorado Map\" Matted Framed Art by Michael Tompsett, Wood Frame",
				"Men's Sueded Sport Coat",
				"Faded Glory - Newborn Girls' Knit Dress and Briefs Set",
				"Ernie Child Halloween Costume",
				"Michael Jackson Figure, Moonwalk",
				"Targus Patterned Stylus, Gray Bubble Fade",
				"Nourison Waverly Global Awakening Imperial Dress Polyester Runner Rug, 2'6\" x 8'",
				"1/8 Carat T.W. Diamond Gold-Plated Stud Earrings",
				"Primo Bottom Loading Hot/Cold Water Dispenser",
				"Ematic Carrying Case for Netbooks 10.3\", Pink",
				"Sally Hansen Diamond Strength No Chip Nail Color, Lavender Marquis",
				"Hanes Essential Tee Shirt Sheet Set",
				"SodaStream Genesis Home Soda Maker Starter Kit",
				"Wilson Pro Stock Game Model Dustin Pedroia 11.5\" Infield Left-Handed Baseball Glove",
				"Disney Toy Story Montage Fathead",
				"Naxa Portable CD Player with AM/FM Radio, Black, NPB251",
				"Kent Infant Baby Safe Vest - Type II",
				"Swan House WRHC120050 50' Blue Self Coiling Water Hose",
				"LEGO Star Wars - Slave I Starship",
				"ZyXEL PLA4101 200 Mbps Mini Powerline Ethernet Adapter",
				"3 Year Service Plan for Musical Instruments and Equipment $200 - $299.99",
				"Rawlings Quad Chair, Pittsburgh Panthers",
				"Jif Reduced Fat Creamy Peanut Butter, 16 oz",
				"Doctor Who Levitating Time Lords Spinning Tardis",
				"Bonus 5x7 Mounted Photo w/Select Premium Canvas Purchase",
				"2 Year Drop, Spill and Cracked Screen Replacement Plan for a Portable GPS $100 - $149.99",
				"Casio Exilim EX-Z80 Vivid Pink 8.1 MP Digital Camera w/ 3x Optical Zoom & YouTube Capture",
				"Microsoft Windows 7 Professional Upgrade",
				"Kensington KeyFolio Executive and Mobile Organizer for iPad and iPad 2",
				"No Boundaries - Juniors Belted Button Neck Tunic",
				"Miller Girl in the Moon Dart Cabinet Includes Darts/Board",
				"Trademark Fine Art \"In Motion\" Canvas Art by Martha Guerra, 16x24",
				"Disney Princess Hide N Play Tent",
				"Whitmor Double PVC Laundry Sorter, White",
				"Hori Super Mario Hard Pouch (DS)",
				"Trademark Fine Art 'Grounded' Canvas Art by Ariane Moshayedi",
				"Bird B Gone MMRTH1 Hawk Decoy Bird Deterrent",
				"Dushi Wooden Push 'n Play Doll Carrier",
				"Miss Tina - Women's Stretch Skinny Jeans",
				"Crunch P500.2 250-Watt 2-Channel Car Power Amplifier",
				"Schwinn Deluxe Cruiser Bike Seat",
				"Trademark Fine Art 'Zen Flowers' Canvas Art by Kathie McCurdy",
				"Flash Furniture Hercules Imagination Series Leather Ottoman, Black",
				"Safavieh Swirl Round Wall Mirror",
				"NFL - Men's Chicago Bears G-III PVC Embroidered Heavyweight Jacket",
				"Disney Princess Baby Cinderella with 9 Royal Accessories Playset",
				"Luvs Ultra Clean Wipes, 72 sheets",
				"MLB Los Angeles Angels Mike Trout 10\" Plush Doll",
				"Safavieh Lyndhurst Victoria Polypropylene Runner Rug",
				"Susie Rose Juniors Belted Ruffle Front Blouse",
				"Bowens Bay Woven Rectangle Area Rug",
				"Littlest Pet Shop Yorkie and Guinea Pig",
				"Just for Men Autostop Hair Color Application Kit",
				"Decorative Panel for 30\" to 37\" Televisions, Mocha",
				"Tripp Lite B126-004 HDMI Over CAT-5/6 4-Port Transmitter",
				"Classroom Keepers Mail Box and Literature Organizer, 10 Slot Sorter",
				"Intellinet 525336 Nano 150N Wireless USB Adapter",
				"Kufo Seco 7.5 HP 5,260 CFM 3-Phase 220V/440V Vertical Bag Dust Collector",
				"Air Hogs Remote-Controlled Heli Cage, Yellow/Blue",
				"Logo Chair NCAA Louisiana Lafayette Sweatshirt Blanket",
				"Boss Audio 10\" Diablo Series Low Profile Subwoofer (One Subwoofer)",
				"Aroma 5-Quart Multi-Function Electric Slow Cooker",
				"Wise Pontoon Fold-Down Seat, Grey/Red/Charcoal",
				"Aunt Lydia's Crochet Cotton",
				"Digipower SP-AC501 Android and BlackBerry Wall Charger",
				"PureGear Slim Shell Case for iPhone 5/5s",
				"Ninja Kitchen System Pulse",
				"Party Animal Alabama Applique Banner Flag",
				"Trademark Fine Art \"Frog's Lunch\" Canvas Art by Sylvia Masek",
				"Holiday Time Pre-Lit 4' Indiana White Artificial Christmas Tree, Blue Lights",
				"Samsung 90W Slim Universal Power Adapter",
				"Mohawk Home Zig Zag Stripe Teal Nylon Rug, Multi-Colored",
				"Divatex 6-Piece Bath Towel Set",
				"Trademark Art \"Painted Tiger Blood Red\" Matted Framed Art by Budi Satria Kwan",
				"Trademark Fine Art \"Old Cabin\" Canvas Wall Art by David Lloyd Glover",
				"Clash of the Titans Dlx Calibos Adult Halloween Costume",
				"NCAA Applique Bedding Comforter Set with Sheets, Boise State",
				"26\" Airwalk Clipper Women's Cruiser Bike",
				"Gateway SX2380-UR308 Small Form Factor Desktop PC with AMD A4-5300 Accelerated Processor, 4GB Memory, 1TB Hard Drive and Windows 8 (Monitor Not Included)",
				"Kelkay Stonetouch Warrior, 37\"",
				"Trademark Fine Art \"Big White Flower\" Canvas Wall Art by Kathie McCurdy",
				"Olay Pro-X Eye Restoration Complex 0.5 Oz",
				"Hanes Easy Comfort Body Pillow with Removable Pillow Cover, Blue Stripes",
				"iGo BN00289-0001 MicroJuice Micro USB Car Charger"};
		WordFilter oWordFilter = new WordFilter();
		for (String line : lines) {
			String newline = oWordFilter.removeUnwantedWordsAndStandardizeUnits(line);
			System.out.println(line);
			System.out.println(newline);
		}
	}
}
