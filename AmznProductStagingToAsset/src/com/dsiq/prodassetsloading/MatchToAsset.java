package com.dsiq.prodassetsloading;

public class MatchToAsset {
	WordFilter oWordFilter;
	MatcherUtils utils;
	
	public MatchToAsset() throws Exception {
		utils = null;//new MatcherUtils();
		oWordFilter = new WordFilter();
	}
	
	public void close() throws Exception {
		utils.close();
	}
	
	public double compareStrings(String s10, String s20, boolean removeStopWords) throws Exception {
		if (!removeStopWords) {
			oWordFilter.removeUnwantedWords = false;                                                                                                                                                                                                                                                                                                                                                                                                                
		} else {
			oWordFilter.removeUnwantedWords = true;
		}
		String s1 = s10;
		String s2 = s20;
		try {
			s1 = oWordFilter.removeUnwantedWordsAndStandardizeUnits(s10);
			s2 = oWordFilter.removeUnwantedWordsAndStandardizeUnits(s20);
			//System.out.println("STRING PROCESSING SUCCESSFUL");
		} catch (java.lang.ArrayIndexOutOfBoundsException e) {
			System.out.println("BAD STRING FOUND: " + s20 + "\n" + "or" + s10 + "\n");
		} catch (java.lang.NumberFormatException e) {
			System.out.println("\"java.lang.NumberFormatException: multiple points\" on inputs " + s10 + " and " + s20 + "\n");
		}
		try {
			return LetterPairSimilarity.compareStrings(s1, s2);
		} catch (java.lang.NegativeArraySizeException e) {
			System.out.println("LETTER PAIR ERROR ON: " + s20 + " and " + s10 + "\n");
			return 0.0;
		} 
	}
	
	
	public double getPriceMatchingScore(double p1, double p2) {
		double mean = 0.5*(p1 + p2);
		double arg = (p1 - p2)/mean;
		arg *= arg;
		double priceMatchingScore = Math.exp(-arg);
		return priceMatchingScore;
	}

}
