package com.dsiq.prodassetsloading;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocumentList;

import com.google.common.base.Strings;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.UUID;
public class MatcherUtils {
	static final String SOLR_BASE_URL = "http://blvdevhdp05:8983/solr/collection1";
	Connection con;
	HttpSolrServer solr;
	String[] assetFieldValues;
	Configuration configuration;



	// Symbolic staging position names

	static final int STAGING_DUMMY = 0;
	static final int STAGING_A = 1;
	static final int STAGING_A_UPC = 2;
	static final int STAGING_A_SKU = 3;
	static final int STAGING_A_T = 4;
	static final int STAGING_A_CA = 5;
	static final int STAGING_A_SC = 6;
	static final int STAGING_A_DP = 7;
	static final int STAGING_A_GL = 8;
	static final int STAGING_A_AR = 9;
	static final int STAGING_A_NR = 10;
	static final int STAGING_A_H = 11;
	static final int STAGING_A_L = 12;
	static final int STAGING_A_W = 13;
	static final int STAGING_A_WT = 14;
	static final int STAGING_A_AVT = 15;
	static final int STAGING_A_B = 16;
	static final int STAGING_A_PH = 17;
	static final int STAGING_A_PL = 18;
	static final int STAGING_A_PW = 19;
	static final int STAGING_A_WG = 20;
	static final int STAGING_A_PQ = 21;
	static final int STAGING_A_SZ = 22;
	static final int STAGING_A_CL = 23;
	static final int STAGING_A_CP = 24;
	static final int STAGING_A_LCP = 25;
	static final int STAGING_A_LP = 26;
	static final int STAGING_A_LRP = 27;
	static final int STAGING_A_LUP = 28;
	static final int STAGING_A_SR = 29;
	static final int STAGING_A_PN = 30;
	static final int STAGING_A_PU = 31;
	static final int STAGING_A_ST = 32;
	static final int STAGING_A_MF = 33;
	static final int STAGING_A_MDL = 34;
	static final int STAGING_A_MPN = 35;
	static final int STAGING_A_PD = 36;
	static final int STAGING_A_TS = 37;
	static final int STAGING_A_P = 38;
	static final int STAGING_A_X = 39;


	static final String[] stagingFieldAliases = { "dummy", // 0
		"A", // 1
		"A_UPC", // 2
		"A_SKU", // 3
		"A_T", // 4
		"A_CA", // 5
		"A_SC", // 6
		"A_DP", // 7
		"A_GL", // 8
		"A_AR", // 9
		"A_NR", // 10
		"A_H", // 11
		"A_L", // 12
		"A_W", // 13
		"A_WT", // 14
		"A_AVT", // 15
		"A_B", // 16
		"A_PH", // 17
		"A_PL", // 18
		"A_PW", // 19
		"A_WG", // 20
		"A_PQ", // 21
		"A_SZ", // 22 
		"A_CAL", // 23
		"A_CP", // 24
		"A_LCP", // 25
		"A_LP", // 26
		"A_LRP", // 27
		"A_LUP", // 28
		"A_SR", // 29
		"A_PN", // 30
		"A_PU", // 31
		"A_ST", // 32
		"A_MF", // 33
		"A_MDL", // 34
		"A_MPN", // 35
		"A_PD", // 36
		"A_TS", // 37
		"A_P", // 38
		"A_X" // 39
	};

	enum dbTypes {
		NONE, VARCHAR, UNSIGNED_INT, FLOAT, UNSIGNED_FLOAT, UNSIGNED_DOUBLE, DOUBLE, TIME};

		static dbTypes[] stagingFieldTypes = {
			dbTypes.NONE, // (dummy) 0
			dbTypes.VARCHAR, // A 1
			dbTypes.VARCHAR, // UPC 2
			dbTypes.VARCHAR, // SKU 3
			dbTypes.VARCHAR, // T 4
			dbTypes.VARCHAR,// CA 5
			dbTypes.VARCHAR,// SC 6
			dbTypes.VARCHAR, // DP 7
			dbTypes.VARCHAR, // GL 8
			dbTypes.UNSIGNED_DOUBLE, // AR (average review rating) 9
			dbTypes.UNSIGNED_INT, // NR (number of reviews) 10
			dbTypes.VARCHAR, // H 11
			dbTypes.VARCHAR, // L 12
			dbTypes.VARCHAR, // W 13
			dbTypes.VARCHAR, // WT 14
			dbTypes.VARCHAR, // AVT 15
			dbTypes.VARCHAR, // B 16
			dbTypes.VARCHAR, // PH 17
			dbTypes.VARCHAR, // PL 18
			dbTypes.VARCHAR, // PW 19
			dbTypes.VARCHAR, // WG 20
			dbTypes.VARCHAR, // PQ 21
			dbTypes.VARCHAR, // SZ (size) 22
			dbTypes.VARCHAR, // CL 23
			dbTypes.VARCHAR, // CP 24
			dbTypes.VARCHAR, // LCP 25
			dbTypes.VARCHAR, // LP 26
			dbTypes.VARCHAR, // LRP 27
			dbTypes.VARCHAR, // LUP 28
			dbTypes.VARCHAR, // SR 29
			dbTypes.VARCHAR, // PN 30
			dbTypes.VARCHAR, // PU 31
			dbTypes.VARCHAR, // ST 32
			dbTypes.VARCHAR, // MF 33
			dbTypes.VARCHAR, // MDL 34
			dbTypes.VARCHAR, // MPN 35
			dbTypes.VARCHAR, // PD 36
			dbTypes.VARCHAR, // TS 37
			dbTypes.VARCHAR, // P 38
			dbTypes.VARCHAR  // X 39
		};


		static dbTypes[] assetFieldTypes = {
			dbTypes.NONE, // dummy 0
			dbTypes.VARCHAR, // P 1
			dbTypes.VARCHAR, // S.M 2
			dbTypes.VARCHAR, // S.D 3
			dbTypes.VARCHAR, // S.B 4
			dbTypes.VARCHAR, // S.W 5
			dbTypes.VARCHAR, // S.L 6
			dbTypes.VARCHAR, // S.WI 7
			dbTypes.VARCHAR, // S.H 8
			dbTypes.VARCHAR, // S.C 9
			dbTypes.VARCHAR, // S.Q 10
			dbTypes.VARCHAR, // S.S 11
			dbTypes.VARCHAR, // S.U 12
			dbTypes.FLOAT, // 13
			dbTypes.FLOAT, // 14
			dbTypes.FLOAT, // 15
			dbTypes.VARCHAR, // 16
			dbTypes.VARCHAR, // A.A 17
			dbTypes.VARCHAR,// A.CA 18
			dbTypes.VARCHAR,// A.SC 19
			dbTypes.VARCHAR, // A.DP 20
			dbTypes.VARCHAR, // A.GL 21
			dbTypes.UNSIGNED_DOUBLE, // A.AR (average review rating) 22
			dbTypes.UNSIGNED_INT, // A.NR (number of reviews) 23
			dbTypes.VARCHAR, // A.CP 24
			dbTypes.VARCHAR, // A.LCP 25
			dbTypes.VARCHAR, // A.LP 26
			dbTypes.VARCHAR, // A.LRP 27
			dbTypes.VARCHAR, // A.LUP 28
			dbTypes.VARCHAR, // A.SR 29
			dbTypes.VARCHAR, // A.PN 30
			dbTypes.VARCHAR, // A.PU 31
			dbTypes.VARCHAR, // A.ST 32
			dbTypes.VARCHAR, // A.MPN 33
			dbTypes.VARCHAR, // A.TS 34
			dbTypes.VARCHAR, // A.U 35
			dbTypes.VARCHAR,  // A.UD 36
			dbTypes.VARCHAR  // W.SD 37
		};
		
		// asset symbolic positions
		static final int ASSET_DUMMY = 0;
		static final int ASSET_P = 1;
		static final int ASSET_S_M = 2;
		static final int ASSET_S_D = 3;
		static final int ASSET_S_B = 4;
		static final int ASSET_S_W = 5;
		static final int ASSET_S_L = 6;
		static final int ASSET_S_WI = 7;
		static final int ASSET_S_H = 8;
		static final int ASSET_S_C = 9;
		static final int ASSET_S_Q = 10;
		static final int ASSET_S_S = 11;
		static final int ASSET_S_U = 12;
		static final int ASSET_S_AP = 13;
		static final int ASSET_S_MSRP = 14;
		static final int ASSET_S_PP = 15;
		static final int ASSET_S_GTIN = 16;
		static final int ASSET_A_A = 17;
		static final int ASSET_A_CA = 18;
		static final int ASSET_A_SC = 19;
		static final int ASSET_A_DP = 20;
		static final int ASSET_A_GL = 21;
		static final int ASSET_A_AR = 22;
		static final int ASSET_A_NR = 23;
		static final int ASSET_A_CP = 24;
		static final int ASSET_A_LCP = 25;
		static final int ASSET_A_LP = 26;
		static final int ASSET_A_LRP = 27;
		static final int ASSET_A_LUP = 28;
		static final int ASSET_A_SR = 29;
		static final int ASSET_A_PN = 30;
		static final int ASSET_A_PU = 31;
		static final int ASSET_A_ST = 32;
		static final int ASSET_A_MPN = 33;
		static final int ASSET_A_TS = 34;
		static final int ASSET_A_U = 35;
		static final int ASSET_A_UD = 36;
		static final int ASSET_W_SD = 37;

		static String[] assetFieldAliases = {
			"dummy", // 0
			"P", // 1
			"S_M", // 2
			"S_D", // 3
			"S_B", // 4
			"S_W", // 5
			"S_L", // 6
			"S_WI", // 7
			"S_H", // 8
			"S_C", // 9
			"S_Q", // 10
			"S_S", // 11
			"S_U", // 12
			"S_AP", // 13
			"S_MSRP", // 14
			"S.PP",   // 15
			"S.GTIN",  // 16
			"A_A", // 17
			"A_CA", // 18
			"A_SC", // 19
			"A_DP", // 20
			"A_GL", // 21
			"A_AR", // 22
			"A_NR", // 23
			"A_CP", // 24
			"A_LCP", // 25
			"A_LP", // 26
			"A_LRP", // 27
			"A_LUP", // 28
			"A_SR", // 29
			"A_PN", // 30
			"A_PU", // 31
			"A_ST", // 32
			"A_MPN", // 33
			"A_TS", // 34
			"A_U", // 35
			"A_UD", // 36
            "W_SD" // 37
		};

		static String[] parseValues(ResultSet rs, typelist list) {
			String[] vals = new String[stagingFieldAliases.length];
			for (int i=1;i<stagingFieldAliases.length;i++) {
				vals[i] = getValue(i, rs, list);
			}
			return vals;
		}

		final static SolrDocumentList EMPTY_SOLR_DOCUMENT_LIST = new SolrDocumentList();


		public MatcherUtils() {
			configuration = Configuration.getInstance();
			makeConnection();
			this.solr = new HttpSolrServer(SOLR_BASE_URL);
		}


		public void makeConnection(){
			try {
				try {
					Class.forName("org.apache.phoenix.jdbc.PhoenixDriver");
				} catch (final ClassNotFoundException e) {
					e.printStackTrace();
				}
				final String connectionURL = "jdbc:phoenix:" + JDBC_Driver.zkQuorum;
				this.con = DriverManager.getConnection(connectionURL);
				con.setAutoCommit(true);
			} catch (final SQLException ex) {
				System.out.println("Location LL exception");
				System.err.println("database connection: " + ex.getMessage());
			}
		}


		public SolrDocumentList getSolrMatches(final String prodTitle) {
			QueryResponse response = null;
			SolrDocumentList result = null;
			if (prodTitle==null) {
				response = null;
			} else {

				final SolrQuery query = new SolrQuery();
				query.setQuery(prodTitle);
				query.addFilterQuery("store_s:AMZN");
				query.setFields("id", "store_s", "sku", "name", "cat", "Brand_s",
						"price", "inStoreOnly_i");
				query.set("defType", "edismax");

				try {
					response = solr.query(query);
				} catch (final SolrServerException e) {
					e.printStackTrace();
				}
			}
			if (response == null) {
				result = EMPTY_SOLR_DOCUMENT_LIST;
			} else {
				result = response.getResults();
			}
			return result;
		}

		int num_calls = 1;




		void setPSVal(final PreparedStatement stmt, final String val, final dbTypes type, final int i) {
			try {
				switch (type) {
				case VARCHAR:
					if (val == null)
						stmt.setNull(i, Types.VARCHAR);
					stmt.setString(i, val);
					break;
				case UNSIGNED_INT:
					if (Strings.isNullOrEmpty(val)) {
						stmt.setNull(i, Types.INTEGER);
					} else {
						stmt.setInt(i, Math.abs(Integer.parseInt(val)));
					}
					break;
				case UNSIGNED_FLOAT:
					if (Strings.isNullOrEmpty(val)) {
						stmt.setNull(i, Types.FLOAT);
					} else {
						stmt.setFloat(i, Math.abs(Float.parseFloat(val)));
					}
					break;
				case UNSIGNED_DOUBLE:
					if (Strings.isNullOrEmpty(val)) {
						stmt.setNull(i, Types.DOUBLE);
					} else {
						stmt.setDouble(i, Math.abs(Double.parseDouble(val)));
					}
					break;
				default:
					if (val == null)
						stmt.setNull(i, Types.VARCHAR);
					stmt.setString(i, val);
					break;
				}
			} catch(final Exception e) {
				System.out.println("Exception in setting: " + e.getMessage());
			}
		}

		void addToPreparedStatementStaging(final PreparedStatement stmt, String[] stagingFieldValues) {
			for (int i=1;i<stagingFieldAliases.length;i++) {
				final String val = stagingFieldValues[i];
				final dbTypes type = stagingFieldTypes[i];
				setPSVal(stmt, val, type, i);
			}
		}


		void addToPreparedStatementAsset(final PreparedStatement stmt, String[] assetFieldValues) {
			for (int i=1;i<assetFieldAliases.length;i++) {
				final String val = assetFieldValues[i];
				final dbTypes type = assetFieldTypes[i];
				setPSVal(stmt, val, type, i);
			}
		}

		public static String qMarks(final String[] strs) {
			final StringBuilder sb = new StringBuilder();
			String delimiter = "";
			for (int i=1; i<=strs.length;i++) {
				sb.append(delimiter);
				sb.append("?");
				delimiter = ",";
			}
			return sb.toString();
		}


		public static String valueList(final String[] strs) {
			final StringBuilder sb = new StringBuilder();
			String delimiter = "";
			for (int i=1; i<=strs.length;i++) {
				sb.append(delimiter);
				sb.append(strs[i]);
				delimiter = ",";
			}
			return sb.toString();
		}

		enum typelist {STAGING, ASSET};

		static String getValue(int fieldNumber, ResultSet rs, typelist list) {
			dbTypes type = dbTypes.VARCHAR;
			String val = null;
			try {
				if (list==typelist.STAGING)
					type = stagingFieldTypes[fieldNumber];
				else
					type = assetFieldTypes[fieldNumber];

				switch (type) {
				case VARCHAR:
					val = rs.getString(fieldNumber);
					break;
				case UNSIGNED_INT:
					val = String.valueOf(rs.getInt(fieldNumber));
					break;
				case FLOAT:
				case UNSIGNED_FLOAT:
					String.valueOf(rs.getFloat(fieldNumber));
					break;
				case DOUBLE:
				case UNSIGNED_DOUBLE:
					String.valueOf(rs.getDouble(fieldNumber));
					break;
				default:
					rs.getString(fieldNumber);
					break;
				}
			} catch(Exception e) {
				System.out.println("Location NN exception");
				System.err.println(e.getStackTrace());
				val = null;
			}
			return val;
		}

		public void stagingToAsset(String[] stagingFieldValues, String[] assetFieldValues) {
			assetFieldValues[ASSET_S_M] = stagingFieldValues[STAGING_A_MF];
			assetFieldValues[ASSET_S_D] = stagingFieldValues[STAGING_A_PD];
			assetFieldValues[ASSET_S_W] = stagingFieldValues[STAGING_A_WT];
			assetFieldValues[ASSET_S_L] = stagingFieldValues[STAGING_A_PL];
			assetFieldValues[ASSET_S_WI] = stagingFieldValues[STAGING_A_PW];
			assetFieldValues[ASSET_S_H] = stagingFieldValues[STAGING_A_PH];
			assetFieldValues[ASSET_S_C] = stagingFieldValues[STAGING_A_CL];
			assetFieldValues[ASSET_S_Q] = stagingFieldValues[STAGING_A_PQ];
			assetFieldValues[ASSET_S_S] = stagingFieldValues[STAGING_A_SZ];
			assetFieldValues[ASSET_A_A] = stagingFieldValues[STAGING_A];
		}

		// called if matches to something in asset
		public void amznMatchingToExistingAssetEntry(String[] stagingFieldValues, String[] assetFieldValues, String assetUUID) {
			stagingToAsset(stagingFieldValues, assetFieldValues);
			upsert(stagingFieldValues, assetFieldValues);
		}
		void upsert(String[] stagingFieldValues, String[] assetFieldValues) {
			try {			
				String sqlUpsertStaging = "UPSERT INTO STAGING.AMZN "
						+ valueList(stagingFieldValues)
						+ " VALUES ("
						+ qMarks(stagingFieldValues)
						+ ")";
				String sqlUpsertAsset = "UPSERT INTO ASSET.PRODUCT "
						+ valueList(assetFieldValues)
						+ " VALUES ("
						+ qMarks(assetFieldValues)
						+ ")";

				Connection conn = JDBC_Driver.getConnection();
				PreparedStatement psStaging = conn.prepareStatement(sqlUpsertStaging);
				PreparedStatement psAsset = conn.prepareStatement(sqlUpsertAsset);
				addToPreparedStatementStaging(psStaging, stagingFieldValues);
				addToPreparedStatementAsset(psAsset, assetFieldValues);
				psStaging.executeQuery(sqlUpsertStaging);
				psAsset.executeQuery(sqlUpsertAsset);

			} catch (Exception e) {
				System.out.println("ERROR in amznToExistingAssetEntry");
				e.printStackTrace();
			}
		}

		private String[] initAssetFieldValues(String assetUUID) {
			String[] asset = new String[assetFieldValues.length];
			asset[ASSET_P] = assetUUID;
			return asset;
		}

		public void close(){
			try {
				if (con!=null && !con.isClosed())
					con.close();
			} catch (final SQLException e) {
				System.out.println("Location NN exception");
				e.printStackTrace();
			}
		}

		public void amznNewAssetEntry(String[] stagingFieldValues) {
			String[] assetFieldValues = initAssetFieldValues(UUID.randomUUID().toString());
			stagingToAsset(stagingFieldValues, assetFieldValues);
			upsert(stagingFieldValues, assetFieldValues);
		}


}
