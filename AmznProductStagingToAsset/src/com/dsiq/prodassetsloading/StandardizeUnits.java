package com.dsiq.prodassetsloading;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

public class StandardizeUnits 
{
	static public final int NOT_FOUND = -1;
	static final DecimalFormat decimalFormat = new DecimalFormat("#.##");
	
	Hashtable<String,String> hUnitsAbv;
	String[] unitAbvs;
	String[] standardizedUnitAbvs;
	
	public StandardizeUnits() {
		hUnitsAbv = this.setUnitsAbv();
		unitAbvs = this.getAllUnitAbvs();
		standardizedUnitAbvs = getAllStandardizedUnitAbvs(unitAbvs);
	}
	
	public boolean hasUnitsInTitle(String title) {
		Matcher m = Pattern.compile("(?!=\\d\\.\\d\\.)([\\d.]+)").matcher(title); //Pattern.compile("(\\d+(?:\\.\\d+))").matcher(title);
		while (m.find()) {
			try {
				double dVal = Double.parseDouble(m.group(1));
			} catch (Exception ex) {
				//Do nothing
			}
			return true;
		}
		return false;
	}
	
	public String fixUnitsInTitle(String title_in) 
	{
		if (title_in.length() <= 1) {
			return title_in;
		}
		if (title_in.matches("(.*)\\d+[. ]?[\"']X[ ]?\\d+(.*)")) {
			String[] words = title_in.split(" ");
			for (int i = 0; i < words.length; i++) {
				if (words[i].matches("(.*)\\d+[\"']X\\d+(.*)")) {
					char[] cs = words[i].toCharArray();
					String word_new = "";
					word_new += cs[0];
					word_new += cs[1];
					for (int j = 2; j < cs.length; j++) {
						if (cs[j] == 'X' && Character.isDigit(cs[j - 2]) && (cs[j - 1] == '\'' || cs[j - 1] == '"')) {
							word_new += " X ";
						} else {
							word_new += cs[j];
						}
					}
					words[i] = word_new;
				}
			}
			title_in = words[0];
			for (int i = 1; i < words.length; i++) {
				title_in += " " + words[i];
			}
		}
		String firstword = "firstword ", lastword = " lastword";
		String title = firstword + title_in.toUpperCase() + lastword;
		List<Integer> hitIndexes = new ArrayList<Integer>();
		for (int i = 0; i < unitAbvs.length; i++) {
			if (title.matches("(.*)\\d\\.?[- ]?" + unitAbvs[i] + "\\W(.*)")) {
				hitIndexes.add(i);
			}
		}
		if (hitIndexes.size() == 0) {
			return title_in;
		}
		String[] baseunits = new String[hitIndexes.size()];
		int index = 0;
		for (int i = 0; i < hitIndexes.size(); i++) {
			index = hitIndexes.get(i);
			baseunits[i] = unitAbvs[index];
		}
		String standardizeUnit = standardizeDblUnits(title, baseunits);	
		int begIndex = firstword.length();
		int endIndex = standardizeUnit.length() - lastword.length() + 1;
		return standardizeUnit.substring(begIndex, endIndex);
	}
	
	String standardizeDblUnits(String title_in, String[] baseunits) 
	{
		Hashtable<String,String> hStandardUnits = new Hashtable<String,String>();
		String[] units = new String[baseunits.length*2*4];
		String[] dot = {"", "."};
		String[] filters = {"[0-9]+", "[0-9]+\\.[0-9]+", "\\.[0-9]+", "[0-9]+\\."};
		String standardUnit = null;
		int index = 0;
		for (int i = 0; i < baseunits.length; i++) {
			for (int j = 0; j < 2; j++) {
				for (int k = 0; k < filters.length; k++) {
					units[index] = filters[k] + dot[j] + baseunits[i];
					standardUnit = this.hUnitsAbv.get(baseunits[i]);
					hStandardUnits.put(units[index++], standardUnit);
				}
			}
		}
		double dVal = 0;
		String sVal = null;
		String title = title_in.toUpperCase();
		String[] wordsInTitle = title.split(" ");
		final int LENGTH = wordsInTitle.length;
		for (int j = 0; j < units.length; j++) {
			if (wordsInTitle[0].matches(units[j]) || wordsInTitle[0].matches(units[j] + "\\W")) {
				Pattern p = Pattern.compile("(?!=\\d\\.\\d\\.)([\\d.]+)");
				Matcher m = p.matcher(wordsInTitle[0]);
				if (m.find()) {
					standardUnit = hStandardUnits.get(units[j]);
					wordsInTitle[0] = m.group(1) + "-" + hStandardUnits.get(standardUnit);
				}
				break;
			} else if (wordsInTitle[0].matches("([\\d.]+)'([\\d.]+)\"X?")) {
				int indx = wordsInTitle[0].indexOf('\'');
				String newWord = wordsInTitle[0].substring(0,indx) + "-FT-";
				indx = wordsInTitle[0].indexOf('"');
				newWord += wordsInTitle[0].substring(indx + 1, indx) + "-IN";
				if (wordsInTitle[0].endsWith("X")) {
					newWord += " X";
				}
				wordsInTitle[0] = newWord;
			}
		}
		int numbMatches = 0;
		for (int i = 1; i < LENGTH; i++) {
			for (int j = 0; j < units.length; j++) {
				if (wordsInTitle[i].matches(units[j]) || wordsInTitle[i].matches(units[j] + "\\W")) {
					Matcher m = Pattern.compile("(?!=\\d\\.\\d\\.)([\\d.]+)").matcher(wordsInTitle[i]);
					if (m.find()) {
						dVal = Double.parseDouble(m.group(1));
						sVal = decimalFormat.format(dVal);
						standardUnit = hStandardUnits.get(units[j]);
						wordsInTitle[i] = "" + sVal + "-" + standardUnit;
						numbMatches++;
					}
					break;
				} else if ((wordsInTitle[i - 1] + wordsInTitle[i]).matches(units[j]) || (wordsInTitle[i - 1] + wordsInTitle[i]).matches(units[j] + "\\W")) {
					Matcher m = Pattern.compile("(?!=\\d\\.\\d\\.)([\\d.]+)").matcher(wordsInTitle[i - 1] + wordsInTitle[i]); 
					if (m.find()) {
						dVal = Double.parseDouble(m.group(1));
						sVal = decimalFormat.format(dVal);
						standardUnit = hStandardUnits.get(units[j]);
						wordsInTitle[i - 1] = "" + sVal + "-" + standardUnit;
						wordsInTitle[i] = null;
						numbMatches++;
					}
					break;
				} else if (wordsInTitle[i].matches("([\\d.]+)'([\\d.]+)\"X?")) {
					int indx = wordsInTitle[i].indexOf('\'');
					String newWord = wordsInTitle[i].substring(0,indx) + "-FT-" + wordsInTitle[i].substring(indx + 1, wordsInTitle[i].length() - 1) + "-IN";
					if (wordsInTitle[i].endsWith("X")) {
						newWord += " X";
					}
					wordsInTitle[i] = newWord;
					break;
				}
			}
		}

		if (numbMatches > 0) {
			String newTitle = wordsInTitle[0];
			for (int i = 1; i < LENGTH; i++) {
				if (wordsInTitle[i] != null) {
					newTitle += " " + wordsInTitle[i];
				}
			}
			return newTitle;
		} else {
			return title;
		}
	}
	
	Hashtable<String,String> setUnitsAbv() {
		Hashtable<String,String> hUnitsAbv = new Hashtable<String,String>();
		hUnitsAbv.put("FT", "FT");
		hUnitsAbv.put("'", "FT");
		hUnitsAbv.put("FOOT", "FT");
		hUnitsAbv.put("FEET", "FT");
		hUnitsAbv.put("GAL", "GAL");
		hUnitsAbv.put("GALS", "GAL");
		hUnitsAbv.put("GALLON", "GAL");
		hUnitsAbv.put("GALLONS", "GAL");
		hUnitsAbv.put("IN", "IN");
		hUnitsAbv.put("\"", "IN");
		hUnitsAbv.put("INS", "IN");
		hUnitsAbv.put("INCH", "IN");
		hUnitsAbv.put("INCHES", "IN");
		hUnitsAbv.put("LB", "LB");
		hUnitsAbv.put("LBS", "LB");
		hUnitsAbv.put("POUND", "LB");
		hUnitsAbv.put("POUNDS", "LB");
		hUnitsAbv.put("OZ", "OZ");
		hUnitsAbv.put("OZS", "OZ");
		hUnitsAbv.put("OUNCE", "OZ");
		hUnitsAbv.put("OUNCES", "OZ");
		hUnitsAbv.put("PT", "PT");
		hUnitsAbv.put("PTS", "PT");
		hUnitsAbv.put("PINT", "OZ");
		hUnitsAbv.put("PINTS", "OZ");
		hUnitsAbv.put("QT", "QT");
		hUnitsAbv.put("QUART", "QT");
		hUnitsAbv.put("QUARTS", "QT");
		hUnitsAbv.put("YD", "YD");
		hUnitsAbv.put("YDS", "YD");
		hUnitsAbv.put("YARD", "YD");
		hUnitsAbv.put("YARDS", "YD");
		hUnitsAbv.put("G", "G");
		hUnitsAbv.put("GR", "G");
		hUnitsAbv.put("GRAM", "G");
		hUnitsAbv.put("GRAMS", "G");
		hUnitsAbv.put("KG", "KG");
		hUnitsAbv.put("KILO", "KG");
		hUnitsAbv.put("KILOS", "KG");
		hUnitsAbv.put("KILOGRAM", "KG");
		hUnitsAbv.put("KILOGRAMS", "KG");
		hUnitsAbv.put("L", "L");
		hUnitsAbv.put("LTR", "L");
		hUnitsAbv.put("LITER", "L");
		hUnitsAbv.put("LITRE", "L");
		hUnitsAbv.put("LITERS", "L");
		hUnitsAbv.put("LITRES", "L");
		hUnitsAbv.put("M", "M");
		hUnitsAbv.put("MTR", "M");
		hUnitsAbv.put("METER", "M");
		hUnitsAbv.put("METERS", "M");
		hUnitsAbv.put("MG", "MG");
		hUnitsAbv.put("MGS", "MG");
		hUnitsAbv.put("MILLIGRAM", "MG");
		hUnitsAbv.put("MILLIGRAMS", "MG");
		hUnitsAbv.put("ML", "ML");
		hUnitsAbv.put("MLS", "ML");
		hUnitsAbv.put("MILLILITER", "ML");
		hUnitsAbv.put("MILLILITERS", "ML");
		return hUnitsAbv;
	}
	
	String[] getAllUnitAbvs() {
		Enumeration<String> unit_keys = hUnitsAbv.keys();
		String[] unitAbvs = new String[hUnitsAbv.size()];
		for (int i = 0; unit_keys.hasMoreElements(); i++) {
			unitAbvs[i] = unit_keys.nextElement();
		}
		Arrays.sort(unitAbvs);
		return unitAbvs;
	}
	
	String[] getAllStandardizedUnitAbvs(String[] unitAbvs) {
		String stdUnitAbv = null;
		HashSet<String> hStandardizedUnitAbvs = new HashSet<String>();
		for (int i = 0; i < unitAbvs.length; i++) {
			stdUnitAbv = this.hUnitsAbv.get(unitAbvs[i]);
			if (!hStandardizedUnitAbvs.contains(stdUnitAbv)) {
				hStandardizedUnitAbvs.add(stdUnitAbv);
			}
		}
		Object[] oStdUnitAbvs = hStandardizedUnitAbvs.toArray();
		String[] stdUnitAbvs = new String[oStdUnitAbvs.length];
		for (int i = 0; i < stdUnitAbvs.length; i++) {
			stdUnitAbvs[i] = (String)oStdUnitAbvs[i];
		}
		Arrays.sort(stdUnitAbvs);
		return stdUnitAbvs; 
	}
	
	private static final String[] packUnitsDef = {"PK.[0-9]+", "[0-9]+.PK", "PK[0-9]+", "[0-9]+PK", "PACK.[0-9]+", "[0-9]+.PACK", "PACK[0-9]+", "[0-9]+PACK", "PACKAGE.[0-9]+", "[0-9]+.PACKAGE"};
	public static int[] getPackSize(String[] titles /*titles[0] is the original title, titles[1] is the standardized title (output)*/) 
	{
		if (titles == null) // for testing only
		{
			titles = new String[2];
			titles[0] = "COMPANY ELEGANCE GLITTER PAPER PACK 12\" 12\", 6 SHEET; BIC FASHION BALL PEN 2-PK BUFFALO WING SAUCE-4PK PACK-22 ASSORT PACKAGE 24 5-PACKAGE";
					 //"COMPANY ELEGANCE GLITTER PAPER PACK 12\" 12\", 6 SHEET; BIC FASHION BALL PEN 2 PK BUFFALO WING SAUCE 4PK PACK 22 ASSORT PACKAGE 24 5 PACKAGE";
		}
		int[] packSizes = getIntSize(titles, packUnitsDef);
		return packSizes;
	}
	
	public static int[] getPackSize(String title /*titles[0] is the original title, titles[1] is the standardized title (output)*/) 
	{
		String[] titles = new String[2];
		if (title == null) // for testing only
		{
			titles[0] = "COMPANY ELEGANCE GLITTER PAPER PACK 12\" 12\", 6 SHEET; BIC FASHION BALL PEN 2-PK BUFFALO WING SAUCE-4PK PACK-22 ASSORT PACKAGE 24 5-PACKAGE";
					 //"COMPANY ELEGANCE GLITTER PAPER PACK 12\" 12\", 6 SHEET; BIC FASHION BALL PEN 2 PK BUFFALO WING SAUCE 4PK PACK 22 ASSORT PACKAGE 24 5 PACKAGE";
		} else {
			titles[0] = title;
		}
		int[] packSizes = getIntSize(titles, packUnitsDef);
		return packSizes;
	}
	
	/*******************************************************************
	\d	Any digit, short for [0-9]
	\D	A non-digit, short for [^0-9]
	\s	A whitespace character, short for [ \t\n\x0b\r\f]
	\S	A non-whitespace character, short for [^\s]
	\w	A word character, short for [a-zA-Z_0-9]
	\W	A non-word character [^\w]
	\S+	Several non-whitespace characters
	\b	Matches a word boundary where a word character is [a-zA-Z0-9_].
	see http://www.vogella.com/tutorials/JavaRegularExpressions/article.html
	*******************************************************************/
	static int[] getIntSize(String[] titles, String[] units) 
	{
		//String[] units = {"PK.[0-9]+", "[0-9]+.PK", "PK[0-9]+", "[0-9]+PK", "PACK.[0-9]+", "[0-9]+.PACK", "PACK[0-9]+", "[0-9]+PACK", "PACKAGE.[0-9]+", "[0-9]+.PACKAGE"};
		String title = titles[0].toUpperCase();
		List<String> tokens = new ArrayList<String>();
		for (String unit : units) {
			tokens.add(unit);
		}
		String patternString = "\\b(" + StringUtils.join(tokens, "|") + ")\\b";
		Pattern pattern = Pattern.compile(patternString);
		Matcher matcher = pattern.matcher(title);
		List<String> wordsFound = new ArrayList<String>();
		while (matcher.find()) {
		    //System.out.println(matcher.group(0));
		    wordsFound.add(matcher.group(0));
		}
		if (wordsFound.size() == 0) {
			return null;
		}
		int[] packSizes = new int[wordsFound.size()];
		pattern = Pattern.compile("(\\d+)");
		String wordFound = null;
		String new_title = title;
		for (int n = 0; n < wordsFound.size(); n++) {
			wordFound = wordsFound.get(n);
			matcher = pattern.matcher(wordFound);
			if (matcher.find()) {
				//System.out.println(matcher.group(0));
				packSizes[n] = Integer.parseInt(matcher.group(0));
			} else {
				packSizes[n] = NOT_FOUND;
			}
			if (packSizes[n] != NOT_FOUND) {
				new_title = new_title.replace(wordFound, Integer.toString(packSizes[n]) + "-PACK");
			}
		}
		titles[1] = new_title;	//one of the return values
		return packSizes;
	}
}
