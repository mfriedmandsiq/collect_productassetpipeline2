package amznproducts;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

// import utilities.UtilFunc;

public class ZonCSVPreprocess {
	static final String line_separator = System.getProperty("line.separator");

	public static void main(String[] args) {
		if (args.length!= 2) {
			System.err.println("Usage: java -jar ZonCSVPreprocess.jar INPUT_FILE OUTPUT_FILE");
			System.exit(-1);
		}
		String fileIn = args[0];
		String fileOut = args[1];
		// System.out.println("filein = " + fileIn);
		// System.out.println("!!!!!!!!!!!!!!fileout (v444444444444444444) = " + fileOut);
		preprocessAsinHunterData(fileIn, fileOut);
	}
	
	static String pad(int tabCount) {
		StringBuilder tabs = new StringBuilder();
		for (int i = 1; i<= tabCount; i++) {
			tabs.append('\t');
		}
		// System.out.println("tabs added: " + tabCount);
		return tabs.toString();
	}

	public static void preprocessAsinHunterData(
			String fileIn, String fileOut) {
		final String del_input = "\",\"";
		final String del = "\t";
		final int EXPECTED_COLUMNS = 34;
		// ASIN Title Brand Category Description Word Count Rating Review List Price
		// Price Discount Commission (4%) Sales Rank Available Thumbnail Product
		// URL
		@SuppressWarnings("unused")
		final int ASIN, TITLE, BRAND, CATEGORY, DESCRIPTION, WORD_COUNT, RATING, REVIEW, LIST_PRICE, PRICE, DISCOUNT, COMMISSION, SALES_RANK, AVAILABLE, THUMBNAIL, PRODUCT_URL,
		FULFILLED, SHIPPING, TOTAL_PRICE, PARENT_ASIN, IMAGE1, IMAGE2, IMAGE3, IMAGE4, IMAGE5, IMAGE6,
		IMAGE7, IMAGE8, IMAGE9, IMAGE10, ACTIVE, LSTATUS, LTIME, EXPTIME;
		{
			int n = 0;
			ASIN = n++;
			TITLE = n++;
			BRAND = n++;
			CATEGORY = n++;
			DESCRIPTION = n++;
			WORD_COUNT = n++;
			RATING = n++;
			REVIEW = n++;
			LIST_PRICE = n++;
			PRICE = n++;
			DISCOUNT = n++;
			COMMISSION = n++;
			SALES_RANK = n++;
			AVAILABLE = n++;
			THUMBNAIL = n++;
			PRODUCT_URL = n++;
			FULFILLED = n++;
			SHIPPING = n++;
			TOTAL_PRICE = n++;
			PARENT_ASIN = n++;
			IMAGE1 = n++;
			IMAGE2 = n++;
			IMAGE3 = n++;
			IMAGE4 = n++;
			IMAGE5 = n++;
			IMAGE6 = n++;
			IMAGE7 = n++;
			IMAGE8 = n++;
			IMAGE9 = n++;
			IMAGE10 = n++;
			ACTIVE = n++;
			LSTATUS = n++;
			LTIME = n++;
			EXPTIME = n;
		}
		BufferedReader r = null;
		try {
			r = new BufferedReader(new FileReader(
					fileIn));
		} catch (FileNotFoundException e) {
			System.err.println("file " + fileIn + " not found");
			System.exit(1);
		}
		BufferedWriter w = null;
		try {
			w = new BufferedWriter(new FileWriter(fileOut));
		} catch (IOException e) {
			e.printStackTrace();
		}
		String line;
		try {
			line = r.readLine();
		    System.out.println(line);
		String[] fields = line.split(del_input, -1);
		// String asin = null, title_orig = null, title = null, brand = null, url = null;

		while ((line = r.readLine()) != null) {
			StringBuilder line_out = new StringBuilder();
			fields = line.split(del_input, -1);
			if (fields[0].length() >= 1 )
				fields[0] = fields[0].substring(1);
			if (fields[fields.length-1].length() >= 1)
				fields[fields.length-1] = fields[fields.length-1].substring(0, fields[fields.length-1].length()-2);
			
			String asin = fields[ASIN];
			if (asin==null || asin.isEmpty()) {
				System.out.println("ASIN is null or empty");
				continue;
			}
			line_out.append(asin);
			
			for (int i = TITLE; i <= Math.min(IMAGE10, fields.length-1); i++) {
                String val = removeDoubleQuotes(fields[i]);
                if (val.equals("\",")) {
    				System.out.println("val equals problem");
                	continue;
                }
				line_out.append(del + removeDoubleQuotes(val));
			}
			String[] s = line_out.toString().split("\t",-1);
			int actualColumns = s.length;
			line_out.append(pad(EXPECTED_COLUMNS - actualColumns));
			String[] finalCols = line_out.toString().split(del,-1);
		    int finalColCnt = finalCols.length;
			w.write(line_out.toString());
			w.newLine();
		    System.out.println("finalColCnt = " + finalColCnt);
		}
		w.close();
		r.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static String removeDoubleQuotes(String words) {
		if (words.startsWith("\"") && words.endsWith("\"")) {
			return words.substring(1, words.length() - 1).trim();
		} else {
			return words.trim();
		}
	}

/*	public static String getTitle(String title_orig, String url) {
		return title_orig;
	}*/
}

