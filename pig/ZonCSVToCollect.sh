cd /work/staging/wm/fb/scripts
CSV_DIR="/work/staging/dsiq/products/amzn/incoming/"
ARCHIVE_DIR="/work/staging/dsiq/products/amzn/archive/"
CSV_FILES="$CSV_DIR*.csv"
HDFS_DIR="/staging1/dsiq/products/amzn/incoming/"
for CSV_FILE in $CSV_FILES
do
        PREPROCESSED_FILE="$CSV_FILE.preprocessed"
        java -jar ZonCSVPreprocess.jar "$CSV_FILE" "$PREPROCESSED_FILE"
        echo "CSV_FILE = $CSV_FILE"
        echo "PREPROCESSED_FILE = $PREPROCESSED_FILE"
        chmod 777  "$PREPROCESSED_FILE"
        filename=$(basename "$PREPROCESSED_FILE")
        extension="${filename##*.}"
        echo "filename = $filename"
        cd "$CSV_DIR"
        pwd
        hadoop fs -D fs.permissions.umask-mode=002 -fs hdfs://blvbetahdp02 -put "$filename" "$HDFS_DIR"
        cd /work/staging/wm/fb/scripts
#       mv "$CSV_FILE" "$ARCHIVE_DIR"
#       mv "$PREPROCESSED_FILE" "$ARCHIVE_DIR"
done
pig -D fs.permissions.umask-mode=002 -fs hdfs://blvbetahdp02 -jt http://blvbetahdp03.ds-iq.corp:8021 -param tsvFile="$HDFS_DIR" -F /work/staging/wm/fb/scripts/ZonCSVToCollect.pig
