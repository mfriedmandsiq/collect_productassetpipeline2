REGISTER /opt/cloudera/parcels/CDH/lib/hbase/hbase.jar
REGISTER /opt/cloudera/parcels/CDH/lib/hadoop/hadoop-common.jar
REGISTER /opt/cloudera/parcels/CDH/lib/zookeeper/zookeeper.jar
REGISTER 'lib/phoenix-3.0.0-incubating-client.jar';
set hbase.zookeeper.quorum 'blvbetahdp08.ds-iq.corp';
raw_data = LOAD '$tsvFile' USING PigStorage('\t') AS (
A:chararray
,T:chararray
,CA:chararray
,DT:chararray
,WC:int
,RA:float
,RE:int
,LP:float
,PR:float
,DI:int
,CO:float
,SR:int
,AV:chararray
,TH:chararray
,PU:chararray
,FU:chararray
,SH:float
,TP:chararray
,PA:chararray
,I1:chararray
,I2:chararray
,I3:chararray
,I4:chararray
,I5:chararray
,I6:chararray
,I7:chararray
,I8:chararray
,I9:chararray
,I10:chararray
,ACTIVE:int
,LSTATUS:chararray
,LTIME:chararray
,EXPTIME:chararray
);
-- STORE data INTO 'hbase://COLLECT_PRODUCT.AMZN' USING
-- org.apache.pig.backend.hadoop.hbase.HBaseStorage (
-- 'A:T, A:CA, A:PD, A:AR, A:NR, A:LP, A:LCP, A:SR, A:AVT');

-- STORE data INTO 'hbase://COLLECT_PRODUCT.AMZN/A,A.T,A.CA,A.PD,A.AR,A.NR,A.LP,A.LCP,A.SR,A.AVT' using org.apache.phoenix.pig.PhoenixHBaseStorage('blvbetahdp08.ds-iq.corp','-batchSize 5000');
STORE raw_data INTO 'hbase://COLLECT_PRODUCT.AMZN' using org.apache.phoenix.pig.PhoenixHBaseStorage('blvbetahdp08.ds-iq.corp','-batchSize 5000');

