set hbase.zookeeper.quorum 'blvbetahdp08.ds-iq.corp';
REGISTER ./lib/piggybank.jar;
x = LOAD 'hbase://ASSET.PRODUCT' USING org.apache.pig.backend.hadoop.hbase.HBaseStorage( 'A:*','-loadKey true') AS (P:chararray, A:map[]);
b = FILTER x by P is not null;
c = LIMIT b 10000;
d = FOREACH c GENERATE SUBSTRING(P,32,35),P,A#'A',A#'UPC';
STORE d INTO '$csvFolder' USING org.apache.pig.piggybank.storage.MultiStorage('$csvFolder','0', 'none', '\\t');
