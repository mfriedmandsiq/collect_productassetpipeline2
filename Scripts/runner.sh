
currenttime=$(date -d "today" +"%Y-%m-%d.%H-%M-%S")
echo "****** START ZIP PROCESSING $currenttime"

/usr/bin/java -Xmx8192m -Xms5120m -jar /opt/ds-iq/zip-to-subcategory/ZipToSubcategory.jar -hd hdfs://blvbetahdp02.ds-iq.corp:8020 -hd hdfs://blvbetahdp02.ds-iq.corp:8020 -f result.txt -z staging/wm/asset/dsiq/zip_code_mapping/store_to_zip_big_no_header.csv
/bin/bash /opt/ds-iq/zip-to-subcategory/hbase-result-loader.sh

currenttime=$(date -d "today" +"%Y-%m-%d.%H-%M-%S")
echo "****** FINISH ZIP PROCESSING $currenttime"

