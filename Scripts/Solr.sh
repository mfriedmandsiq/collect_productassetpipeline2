# Run with sudo -u wfbgblvdevapp03   - OR - sudo -u wfbgblvbetaapp04
# argument is directory to process
LINUX_SOLR_CSV="$1"
LINUX_SOLR_CSV_ARCHIVE="${LINUX_SOLR_CSV}archive/"
cd "$1"
for RAW_FILE in part*
do
    mv "$RAW_FILE" "${RAW_FILE}.csv"
done
for CSV_FILE in part*.csv
do
	curl http://blvdevhdp05:8983/solr/collection1/update/csv --data-binary @"$CSV_FILE" -H 'Content-type:text/plain; charset=utf-8'
        cp "$CSV_FILE" "${LINUX_SOLR_CSV_ARCHIVE}/"
done

