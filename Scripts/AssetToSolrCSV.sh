# Run with sudo -u wfbgblvdevapp03   - OR - sudo -u wfbgblvbetaapp04 
echo Starting ...
HDFS_SOLRCSV_DIR="/staging1/dsiq/products/solrcsv"
LINUX_SOLRCSV_DIR="/home/wfbgblvbetaapp04/solrcsv"
#
# Note: Using explicit path due to danger of this operation
sudo -u hdfs hadoop fs -D fs.permissions.umask-mode=002 -fs hdfs://blvbetahdp02 -rm -r "/staging1/dsiq/products/solrcsv"
#
# export PIG_HEAPSIZE=4096
# export HADOOP_OPTS="-Xmx4096m"
pig -D fs.permissions.umask-mode=002 -fs hdfs://blvbetahdp02 -param "csvFolder=$HDFS_SOLRCSV_DIR" -F AssetToSolrCSV.pig
sudo -u hdfs hadoop fs -D fs.permissions.umask-mode=002 -fs hdfs://blvbetahdp02 -ls "/staging1/dsiq/products/solrcsv"
echo A
#
sudo rm -r -f "$LINUX_SOLRCSV_DIR"
echo B
sudo mkdir -p "$LINUX_SOLRCSV_DIR"
echo C
sudo chown wfbgblvbetaapp04 "$LINUX_SOLRCSV_DIR"
echo CC
#sudo chgrp  service^accounts "$LINUX_SOLR_DIR"
echo D
sudo chmod 777 "$LINUX_SOLRCSV_DIR"
echo E
sudo -u hdfs hadoop fs -chmod 777 "$HDFS_SOLRCSV_DIR"
echo F
sudo -u hdfs hadoop fs -chmod 777 "$HDFS_SOLRCSV_DIR/*"
echo G
sudo -u hdfs hadoop fs -fs hdfs://blvbetahdp02 -copyToLocal "$HDFS_SOLRCSV_DIR/part*" "${LINUX_SOLRCSV_DIR}/"
echo H
#
sudo -u hdfs hadoop fs -chmod 777 "$HDFS_SOLRCSV_DIR"
echo I
ls -la "$LINUX_SOLRCSV_DIR"
echo J
echo That is all, folks
# ./Solr.sh "$LINUX_SOLRCSV_DIR"

