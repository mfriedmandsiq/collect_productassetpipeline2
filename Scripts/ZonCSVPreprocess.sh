#!/bin/bash
shopt -s nullglob
CSV_DIR="/work/staging/dsiq/products/amzn/incoming/"
CSV_FILES="$CSV_DIR*.csv"
echo My process  is `ps augx | grep ZonCSVPreprocess`
sudo rm -f /work/staging/dsiq/products/amzn/incoming/*.csv.preprocessed
sudo rm -f /work/staging/dsiq/products/amzn/incoming/*.csv.transferred
sudo chmod --silent 777 "$CSV_FILES"
sudo chown --silent wfbgblvbetaapp04 "$CSV_FILES"
sudo chown --silent wfbgblvbetaapp04 /work/staging/dsiq/products/amzn/incoming/*
chown --silent 777 /work/staging/dsiq/products/amzn/incoming/*
echo
for CSV_FILE in $CSV_FILES
do
       echo "CSV_FILE = $CSV_FILE"
       PREPROCESSED_FILE="$CSV_FILE.preprocessed"
       sudo rm -f "$PREPROCESSED_FILE"
       touch "$PREPROCESSED_FILE"
       sudo chmod --silent 777 "$PREPROCESSED_FILE"
       sudo chmod --silent 777 "$CSV_FILE"
       echo Preprocessing...
       java -jar /opt/ds-iq/zon-to-collect/ZonCSVPreprocess.jar "$CSV_FILE" "$PREPROCESSED_FILE"
       echo "PREPROCESSED_FILE = $PREPROCESSED_FILE"
       sudo chmod --silent 777  "$PREPROCESSED_FILE"
done

