logfile=/opt/ds-iq/zip-to-subcategory/log/zip-to-subcategory.log
if [ -f $logfile ];
then
    actualsize="$(stat -c %s $logfile)";
    msize=90000
    if [ $actualsize -ge $msize ]; then
    echo "Create new log"
    currenttime=$(date -d "today" +"%Y-%m-%d.%H%M%S")
#    newlog="${logfile::${#logfile}-4}-output.$currenttime.log"
    mv "$logfile" "${logfile::${#logfile}-4}-output.$currenttime.log"
    touch $logfile
    fi
fi

fileName=".txt"
for f in $(basename $(hadoop fs -ls /asset/wm/subcategory/queue/*.txt 2>/dev/null | sed '1d;s/  */ /g' | cut -d\  -f8))
do
echo $f

 ch=${f:0:1}
 if [[ $ch == "#" ]]
 then
    echo "Queue has task $f"
    fileName=$(basename $f .txt)
    # take the first from queue
    echo "/asset/wm/subcategory/queue/$fileName.txt"
    echo  "/asset/wm/subcategory/queue/_$fileName.txt"
    break
 fi
done

echo $fileName


