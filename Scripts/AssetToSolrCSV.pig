set hbase.zookeeper.quorum '192.168.210.219';
SET hbase.client.scanner.caching 1000;
-- REGISTER ./lib/piggybank.jar;
REGISTER  /opt/cloudera/parcels/CDH/lib/hbase/hbase-common.jar
REGISTER /opt/cloudera/parcels/CDH/lib/hadoop/hadoop-common.jar
REGISTER /opt/cloudera/parcels/CDH/lib/zookeeper/zookeeper.jar
-- REGISTER lib/phoenix-4.1.0-SNAPSHOT-client.jar
REGISTER lib/piggybank.jar;
DEFINE CSVExcelStorage org.apache.pig.piggybank.storage.CSVExcelStorage; 


x = LOAD 'hbase://ASSET.PRODUCT' USING org.apache.pig.backend.hadoop.hbase.HBaseStorage( 'S:*,A:*,W:*','-loadKey true') AS (P:chararray, S:map[], A:map[], W:map[]);

b = FILTER x by P is not null; 
c = FOREACH b GENERATE P, A#'A' AS A, TRIM(REPLACE(REPLACE(S#'D',',',''),'\\n','')) AS D, REPLACE(S#'B',',','') AS B;

-- STORE d INTO '$csvFolder' USING PigStorage('`');
-- STORE d INTO '$csvFolder' USING org.apache.pig.piggybank.storage.MultiStorage('$csvFolder','0', 'none', ',');
STORE c INTO '$csvFolder' USING CSVExcelStorage(',','NO_MULTILINE','WINDOWS');
